package com.example.usuario.Authenticator;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.usuario.tfgvuforia.R;
import com.example.usuario.tfgvuforia.app.Activities.ActividadAutenticacion;
import com.example.usuario.tfgvuforia.app.Activities.ActividadRegistro;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;

import static android.accounts.AccountManager.KEY_AUTHENTICATOR_TYPES;

/**
 * Created by cgc on 12/06/2018.
 */
public class AccountAuthenticator extends AbstractAccountAuthenticator {

    String url= "http://192.168.1.133:1111:etsidi/login/auth";

    private final Context mContext;

    public AccountAuthenticator(Context context){
        super(context);
        mContext=context;
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse accountAuthenticatorResponse, String s) {
       throw new UnsupportedOperationException();
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse accountAuthenticatorResponse,
                             String accountType,
                             String authTokenType,
                             String[] strings,
                             Bundle bundle) throws NetworkErrorException {

       final Intent intent= new Intent(mContext, ActividadAutenticacion.class);
       intent.putExtra("com.example.usuario.tfgvuforia", accountType);
       intent.putExtra("authorization_code", authTokenType);
       intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, accountAuthenticatorResponse);
       final Bundle bundle1= new Bundle();
       bundle1.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle1;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) throws NetworkErrorException {

       /* if (options != null && options.containsKey(AccountManager.KEY_PASSWORD)) {
            final String password = options.getString(AccountManager.KEY_PASSWORD);
            final Bundle result = new Bundle();
            result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, verified);
            return result;
        }
        // Launch AuthenticatorActivity to confirm credentials
        final Intent intent = new Intent(mContext, ActividadAutenticacion.class);
        intent.putExtra(ActividadAutenticacion.PARAM_USERNAME, account.name);
        intent.putExtra(ActividadAutenticacion.PARAM_CONFIRMCREDENTIALS, true);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE,
                response);
        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;*/
      return null;
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account,
                               String authTokenType, Bundle bundle) throws NetworkErrorException {

        AccountManager am = AccountManager.get(mContext);
        String authToken = am.peekAuthToken(account, authTokenType);

        if(/*!authTokenType.equals("authorization_code")*/ authToken==null){ //SE SUPONE QUE ES AUTHTOKEN_TYPE
            Log.d("PRUEBA", "TOKEN NULL");
            final Bundle result = new Bundle();
            result.putString(AccountManager.KEY_ERROR_MESSAGE, "invalid authTokenType");
            return result;
        }

      //  final AccountManager am= AccountManager.get(mContext);
        //final String password = am.getPassword(account);

        if (authToken != null) {
            Log.d("PRUEBA", "TOKEN  NO NULL");
                final Bundle result = new Bundle();
                result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
                result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
                //result.putString(AccountManager.KEY_AUTHTOKEN, password);
            result.putString(AccountManager.KEY_AUTHTOKEN, authToken);
                return result;
            }

        // the password was missing or incorrect, return an Intent to an
        // Activity that will prompt the user for the password.
        final Intent intent = new Intent(mContext, ActividadAutenticacion.class);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
        intent.putExtra("com.example.usuario.tfgvuforia", account.type);
        intent.putExtra("authorization_code", authTokenType);
        final Bundle bundle1 = new Bundle();
        bundle1.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle1;
    }

    @Override
    public String getAuthTokenLabel(String s) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse accountAuthenticatorResponse, Account account, String s, Bundle bundle) throws NetworkErrorException {

      Log.d("UPDATE CREDENTIALS", "HOLA");
        final Intent intent = new Intent(mContext, ActividadAutenticacion.class);
        intent.putExtra(ActividadAutenticacion.PARAM_USERNAME, account.name);
        intent.putExtra(ActividadAutenticacion.PARAM_AUTHTOKEN_TYPE,
                "authorization_code");
        intent.putExtra(ActividadAutenticacion.PARAM_CONFIRMCREDENTIALS, false);
        final Bundle bundle1 = new Bundle();
        bundle1.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle1;
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse accountAuthenticatorResponse, Account account, String[] strings) throws NetworkErrorException {

        Log.d("HAS FEATURES", "HOLA");
        final Bundle result = new Bundle();
        result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false);
        return result;
    }
}
