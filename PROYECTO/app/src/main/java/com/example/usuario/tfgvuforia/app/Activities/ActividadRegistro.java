package com.example.usuario.tfgvuforia.app.Activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.usuario.ConexionBBDD.AppController;
import com.example.usuario.tfgvuforia.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cgc on 08/02/2018.
 */

public class ActividadRegistro extends Activity{

    private static final String LOGTAG = "registerActivity";

    //LAYOUT
    private Button btnRegister;
    private EditText inputEmail;
    private EditText inputPassword;
    private EditText inputName;
    private EditText inputLastName;
    private Typeface font;

    //CONFIGURACION
    private String ip;
    private String url;
    private int role;
    private boolean resultadoAl, resultadoPro;

    private String name;
    private String lastName;
    private String email;
    private String password;

    private AccountManager mAccountManager;
    private Account mCurrentAccount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registrolayout);

        role=2;

        font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");

        inputEmail = (EditText) findViewById(R.id.txtEmail);
        inputEmail.setTypeface(font);
        inputPassword = (EditText) findViewById(R.id.txtPass);
        inputPassword.setTypeface(font);
        inputName = (EditText) findViewById(R.id.txtName);
        inputName.setTypeface(font);
        inputLastName = (EditText) findViewById(R.id.txtLastName);
        inputLastName.setTypeface(font);

        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnRegister.setTypeface(font);

        mAccountManager=AccountManager.get(this);

        //CONTENIDO DE CONFIGURACION
        ip = ((AppController) this.getApplicationContext()).getIp();
        url="http://" + ip + ":8081/auth/";

        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                email = inputEmail.getText().toString();
                password = inputPassword.getText().toString();
                name = inputName.getText().toString();
                lastName= inputLastName.getText().toString();

                createAccount();
            }
        });
    }

    private void createAccount(){

        email = inputEmail.getText().toString();
        password = inputPassword.getText().toString();
        name = inputName.getText().toString();
        lastName= inputLastName.getText().toString();

        String emailCorrectoAl, emailCorrectoPro;
        emailCorrectoAl = "@alumnos.upm.es";
        emailCorrectoPro = "@upm.es";
        resultadoAl = email.contains(emailCorrectoAl);
        resultadoPro = email.contains(emailCorrectoPro);
        if(resultadoAl)
            role=0;
        else if(resultadoPro)
            role=1;
        ((AppController) getApplication()).setRole(role);
        ((AppController) getApplication()).setUser(email);

        if(resultadoPro==true || resultadoAl==true) {
            if(!((AppController) this.getApplicationContext()).getMockup()){
                mCurrentAccount=new Account(email, "authorization_code");
                Bundle userData=new Bundle();
                boolean addAccountResult =mAccountManager.addAccountExplicitly(mCurrentAccount, password, userData);

                if(addAccountResult){
                    Log.d("PRUEBA", "CUENTA CREADA");
                    addPreference();
                    Intent itemintent = new Intent(ActividadRegistro.this, ActividadOpciones.class);
                    finish();
                    ActividadRegistro.this.startActivity(itemintent);
                }
                else{
                    Log.d("PRUEBA", "CUENTA ERROR");
                }
            } else{
                //MOCKUP
                /**************/
                LayoutInflater inflater= getLayoutInflater();
                View layout= inflater.inflate(R.layout.toastlayout, (ViewGroup) findViewById(R.id.toastlayout));
                TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
                txtmsg.setText(R.string.RegistroOK);
                txtmsg.setTypeface(font);
                Toast toast1 = new Toast(getApplicationContext());
                toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                toast1.setView(layout);
                toast1.show();
                toast1.setDuration(Toast.LENGTH_SHORT);

                Intent itemintent = new Intent(ActividadRegistro.this, ActividadOpciones.class);
                finish();
                ActividadRegistro.this.startActivity(itemintent);
                /*************/
            }
        } else {
            LayoutInflater inflater= getLayoutInflater();
            View layout= inflater.inflate(R.layout.toastlayout, (ViewGroup) findViewById(R.id.toastlayout));
            TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
            txtmsg.setText(R.string.ERRORextEmail);
            txtmsg.setTypeface(font);
            Toast toast1 = new Toast(getApplicationContext());
            toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
            toast1.setView(layout);
            toast1.show();
            toast1.setDuration(Toast.LENGTH_SHORT);
        }
    }

    private void addPreference(){
        mAccountManager.setUserData(mCurrentAccount, "name", name);
        mAccountManager.setUserData(mCurrentAccount, "lastName", lastName);
        Log.d("PRUEBA", "PREF AÑADIDAS");
    }

}