package com.example.usuario;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.usuario.tfgvuforia.R;

import java.util.ArrayList;
import java.util.Map;

public class ExpandibleSimple extends BaseExpandableListAdapter {

    private ArrayList<String> listTitulo;
    private Map<String, ArrayList<String>> mapChild;
    private Context context;
    private int tipo;

    public ExpandibleSimple(Context context, ArrayList<String> listTitulo, Map<String, ArrayList<String>> mapChild, int tipo){
        this.context=context;
        this.listTitulo=listTitulo;
        this.mapChild=mapChild;
        this.tipo=tipo;
    }

    @Override
    public int getGroupCount() {
        return listTitulo.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return mapChild.get(listTitulo.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return listTitulo.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return mapChild.get(listTitulo.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        final Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/futura.ttf");

        String Nombre = (String) getGroup(i);
        view= LayoutInflater.from(context).inflate(R.layout.expandablegroup, null);
        TextView textViewGroup = (TextView) view.findViewById(R.id.ExpanGroup);
        textViewGroup.setText(Nombre);
        textViewGroup.setTypeface(font);
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        final Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/futura.ttf");

        if(tipo==0){
            String mensaje = (String) getChild(i,i1);
            view= LayoutInflater.from(context).inflate(R.layout.expandablechildavisos, null);
            TextView textViewChild = view.findViewById(R.id.ExpanChildAvisos);
            textViewChild.setText(mensaje);
            textViewChild.setTypeface(font);
        }
        else if(tipo==1)
        {
            String web = (String) getChild(i,i1);
            view= LayoutInflater.from(context).inflate(R.layout.expandablechilddocumentos, null);
            WebView webView = view.findViewById(R.id.Webview);
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webView.setWebViewClient(new WebViewClient());
            webView.loadUrl(web);

        }

        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
