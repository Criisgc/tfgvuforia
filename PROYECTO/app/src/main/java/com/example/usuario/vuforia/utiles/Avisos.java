package com.example.usuario.vuforia.utiles;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import com.example.usuario.tfgvuforia.app.Activities.Renderizador;

import java.io.IOException;
import java.nio.Buffer;
import java.util.Vector;

/**
 * Created by cgc on 05/04/2018.
 */

public class Avisos {
    private static final String LOGTAG = "Tablon avisos";

    private Vector<Objeto> VectorObjetos = new Vector<>();

    public void Load() throws IOException
    {
        LectorTxt load = new LectorTxt();
        load.loadModel(Renderizador.asset, "avisos.txt", VectorObjetos);
        //Log.d(LOGTAG, "dentro de load, tamaño objetos:" + VectorObjetos.size());
        for(int i = 0; i < VectorObjetos.size(); ++i)
        {
            Objeto objeto = VectorObjetos.elementAt(i);
            objeto.rewind();
        }
    }

    public Buffer getVertices(int nObjeto)
    {
        return getData(Objeto.DATA_TYPE.TYPE_VERTEX, nObjeto);
    }

    public int getNumObjectVertex(int nObjeto)
    {
        Objeto objeto = VectorObjetos.elementAt(nObjeto);
        return   objeto.getNumObjectVertex();
    }

    public int getNumObjectIndex(int nObjeto)
    {
        Objeto objeto = VectorObjetos.elementAt(nObjeto);
        return   objeto.getNumObjectIndex();
    }

    public Buffer getIndices(int nObjeto)
    {
        return getData(Objeto.DATA_TYPE.TYPE_INDEX, nObjeto);
    }

    public Buffer getTexCoords(int nObjeto)
    {
        return getData(Objeto.DATA_TYPE.TYPE_TEXTURE, nObjeto);
    }

    public Buffer getData(Objeto.DATA_TYPE dataType, int nObjeto)
    {
        Objeto objeto = VectorObjetos.elementAt(nObjeto);
        Buffer result = objeto.getBuffer(dataType);
        return result;
    }

    public void Draw(float[] modelViewMatrixAviso, float[] projectionMatrix, Vector<Textura> mTextures)
    {
        int textureIndex=0;
        int endObject = VectorObjetos.size() ;

        //TRANSFORMACIONES
        float[] modelViewMatrixObj1 = modelViewMatrixAviso;
        float[] modelViewMatrixObj2 = modelViewMatrixAviso;
        float[] modelViewMatrixObj3 = modelViewMatrixAviso;
        float[] modelViewMatrixObj4 = modelViewMatrixAviso;
        float[] modelViewMatrixObj5 = modelViewMatrixAviso;
        float[] modelViewMatrixObj6 = modelViewMatrixAviso;
        float[] modelViewMatrixObj7 = modelViewMatrixAviso;
        float[] modelViewMatrixObj8 = modelViewMatrixAviso;

        float[] projectionMatrixObj1 = projectionMatrix;
        float[] projectionMatrixObj2 = projectionMatrix;
        float[] projectionMatrixObj3 = projectionMatrix;
        float[] projectionMatrixObj4 = projectionMatrix;
        float[] projectionMatrixObj5 = projectionMatrix;
        float[] projectionMatrixObj6 = projectionMatrix;
        float[] projectionMatrixObj7 = projectionMatrix;
        float[] projectionMatrixObj8 = projectionMatrix;

        Vector<float[]> modelViewProjection= new Vector<>(8);

        float[] modelViewProjectionObj1 = new float[16];
        float[] modelViewProjectionObj2 = new float[16];
        float[] modelViewProjectionObj3 = new float[16];
        float[] modelViewProjectionObj4 = new float[16];
        float[] modelViewProjectionObj5 = new float[16];
        float[] modelViewProjectionObj6 = new float[16];
        float[] modelViewProjectionObj7 = new float[16];
        float[] modelViewProjectionObj8 = new float[16];

       //corcho
        float objectscale1=0.0006f;
        Matrix.scaleM(modelViewMatrixObj1, 0, objectscale1, objectscale1, objectscale1);
        Matrix.translateM(modelViewMatrixObj1, 0, -200.0f, 180.0f, 0.0f);
        Matrix.rotateM(modelViewMatrixObj1,0, -90.0f, 0.0f, 0.0f, 1.0f);
        Matrix.multiplyMM(modelViewProjectionObj1, 0, projectionMatrixObj1, 0, modelViewMatrixObj1, 0);
        modelViewProjection.add(modelViewProjectionObj1);
        //folio
        float objectscale2=0.8f;
        Matrix.scaleM(modelViewMatrixObj2, 0, objectscale2, objectscale2, 1);
        Matrix.translateM(modelViewMatrixObj2, 0, 10.0f, 10.0f, 20.0f);
        Matrix.multiplyMM(modelViewProjectionObj2, 0, projectionMatrixObj2, 0, modelViewMatrixObj2, 0);
        modelViewProjection.add(modelViewProjectionObj2);
        //exclamacion
        float objectscale3=0.7f;
        Matrix.scaleM(modelViewMatrixObj3, 0, objectscale3, objectscale3,objectscale3*0.1f);
        Matrix.translateM(modelViewMatrixObj3, 0, -10.0f, -10.0f, 120.0f);
        Matrix.multiplyMM(modelViewProjectionObj3, 0, projectionMatrixObj3, 0, modelViewMatrixObj3, 0);
        modelViewProjection.add(modelViewProjectionObj3);
        //punto
        float objectscale4=0.2f;
        Matrix.translateM(modelViewMatrixObj4, 0, 55.0f, 40.0f, 0.0f);
        Matrix.scaleM(modelViewMatrixObj4, 0, objectscale4, objectscale4, 1);
        Matrix.multiplyMM(modelViewProjectionObj4, 0, projectionMatrixObj4, 0, modelViewMatrixObj4, 0);
        modelViewProjection.add(modelViewProjectionObj4);
        //celo
        float objectscale5=2.5f;
        Matrix.scaleM(modelViewMatrixObj5, 0, objectscale5, objectscale5, objectscale5);
        Matrix.translateM(modelViewMatrixObj5, 0, 40.0f, -100.0f, 0.0f);
        Matrix.rotateM(modelViewMatrixObj5,0, -40.0f, 0.0f, 0.0f, 1.0f);
        Matrix.multiplyMM(modelViewProjectionObj5, 0, projectionMatrixObj5, 0, modelViewMatrixObj5, 0);
        modelViewProjection.add(modelViewProjectionObj5);
        //celo2
        Matrix.rotateM(modelViewMatrixObj6,0, 40.0f, 0.0f, 0.0f, 1.0f);
        Matrix.translateM(modelViewMatrixObj6, 0, -320.0f, -10.0f, 0.0f);
        Matrix.rotateM(modelViewMatrixObj6,0, 40.0f, 0.0f, 0.0f, 1.0f);
        Matrix.multiplyMM(modelViewProjectionObj6, 0, projectionMatrixObj6, 0, modelViewMatrixObj6, 0);
        modelViewProjection.add(modelViewProjectionObj6);
        //celo3
        Matrix.rotateM(modelViewMatrixObj7,0, -40.0f, 0.0f, 0.0f, 1.0f);
        Matrix.translateM(modelViewMatrixObj7, 0, 0.0f, 200.0f, 0.0f);
        Matrix.rotateM(modelViewMatrixObj7,0, -30.0f, 0.0f, 0.0f, 1.0f);
        Matrix.multiplyMM(modelViewProjectionObj7, 0, projectionMatrixObj7, 0, modelViewMatrixObj7, 0);
        modelViewProjection.add(modelViewProjectionObj7);
        //celo4
        Matrix.rotateM(modelViewMatrixObj7,0, 30.0f, 0.0f, 0.0f, 1.0f);
        Matrix.translateM(modelViewMatrixObj7, 0, 340.0f, -30.0f, 50.0f);
        Matrix.rotateM(modelViewMatrixObj7,0, 60.0f, 0.0f, 0.0f, 1.0f);
        Matrix.multiplyMM(modelViewProjectionObj8, 0, projectionMatrixObj8, 0, modelViewMatrixObj8, 0);
        modelViewProjection.add(modelViewProjectionObj8);

        for (int i = 0; i < endObject; ++i)
        {
            if(i==7)  textureIndex = Objeto.COLOR_VERDE;
            if(i==6)  textureIndex = Objeto.COLOR_AMARILLO;
            if(i==5)  textureIndex = Objeto.COLOR_MORADO;
            if(i==4)  textureIndex = Objeto.COLOR_AZULCLARO;
            if(i==3)  textureIndex = Objeto.COLOR_ROJO;
            if(i==2)  textureIndex = Objeto.COLOR_ROJO;
            if(i==1)  textureIndex = Objeto.COLOR_BLANCO;
            if(i==0)  textureIndex = Objeto.COLOR_MARRON;

            GLES20.glDisable(GLES20.GL_CULL_FACE);
            GLES20.glVertexAttribPointer(Renderizador.vertexHandle, 3, GLES20.GL_FLOAT, false, 0, getVertices(i));
            GLES20.glVertexAttribPointer(Renderizador.textureCoordHandle, 2, GLES20.GL_FLOAT, false, 0, getTexCoords(i));

            GLES20.glEnableVertexAttribArray(Renderizador.vertexHandle);
            GLES20.glEnableVertexAttribArray(Renderizador.textureCoordHandle);

            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextures.get(textureIndex).mTextureID[0]);

            GLES20.glUniform1i(Renderizador.texSampler2DHandle, 0);

            GLES20.glUniformMatrix4fv(Renderizador.mvpMatrixHandle, 1, false, modelViewProjection.elementAt(i), 0);

            GLES20.glDrawElements(GLES20.GL_TRIANGLES, getNumObjectIndex(i), GLES20.GL_UNSIGNED_INT, getIndices(i));

            GLES20.glDisableVertexAttribArray(Renderizador.vertexHandle);
            GLES20.glDisableVertexAttribArray(Renderizador.textureCoordHandle);
        }
    }
}
