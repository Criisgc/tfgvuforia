package com.example.usuario.tfgvuforia.app.Activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.usuario.ConexionBBDD.AppController;
import com.example.usuario.tfgvuforia.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by cgc on 13/06/2018.
 */

public class ActividadHorariosLaboratorio extends Activity {

    private static final String LOGTAG = "HorarioLaboActivity";

   //LAYOUT
    private TextView tituloHorariosLaboratorio;
    private TextView horariosLaboratorio;

    private String Laboratorio;
    private String url;

    //CONFIGURACION
    private String ip;

    //BBDD
    private JSONArray data;
    private String titulacion;
    private String codAsignatura;
    private String asignatura;
    private String grupo;
    private String dia;
    private String horaIni;
    private String horaFin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.horarioslaboratoriolayout);

        final Typeface font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");

        tituloHorariosLaboratorio = (TextView) findViewById(R.id.tituloHorarioLaboratorio);
        tituloHorariosLaboratorio.setTypeface(font);
        horariosLaboratorio = (TextView) findViewById(R.id.textoHorariosLaboratorio);
        horariosLaboratorio.setTypeface(font);

        Laboratorio = ((AppController) this.getApplicationContext()).getDestinatario();

        //CONTENIDO DE CONFIGURACION
        ip=((AppController) this.getApplicationContext()).getIp();
        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=" +  Laboratorio + "&pagina=0";

        getLaboratorio();
    }

    public void getLaboratorio() {

        //MOCKUP
        /**************/
        if(((AppController) this.getApplicationContext()).getMockup()){
            if(Laboratorio.equals("Electrónica")){
                horariosLaboratorio.setText(
                        getString(R.string.codAsignaturaJSON) + "1021" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Electronica Analógica" + "\n" +
                        getString(R.string.GrupoJSON) + "Grupo1" + "\n" +
                        getString(R.string.DiaJSON) + "Martes" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "15:00" + "\n" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "17:00" + "\n" +
                        getString(R.string.GrupoJSON) + "Grupo2" + "\n" +
                        getString(R.string.DiaJSON) + "Martes" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "17:00" + "\n" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "19:00" + "\n" +
                        getString(R.string.GrupoJSON) + "Grupo3" + "\n" +
                        getString(R.string.DiaJSON) + "Jueves" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "15:00" + "\n" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "17:00" + "\n");
            }
            else if(Laboratorio.equals("Informática")){
                horariosLaboratorio.setText(getString(R.string.codAsignaturaJSON) + "1053" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Informática Industrial" + "\n" +
                        getString(R.string.GrupoJSON) + "Grupo1" + "\n" +
                        getString(R.string.DiaJSON) + "Lunes" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "9:00" + "\n" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "11:00" + "\n" +
                        getString(R.string.GrupoJSON) + "Grupo2" + "\n" +
                        getString(R.string.DiaJSON) + "Martes" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "9:00" + "\n" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "11:00" + "\n" +
                        getString(R.string.GrupoJSON) + "Grupo3" + "\n" +
                        getString(R.string.DiaJSON) + "Miércoles" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "9:00" + "\n" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "11:00" + "\n");;
            }
        }
        /**************/


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(LOGTAG, "dentro de onResponse, url:" + url);
                        String textoFinal=" ";
                        try {
                            data = response.getJSONArray("content");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject obj = data.getJSONObject(i);
                                titulacion = obj.getString("titulacion");
                                codAsignatura = obj.getString("codAsignatura");
                                asignatura = obj.getString("asignatura");
                                grupo = obj.getString("grupo");
                                dia = obj.getString("dia");
                                horaIni = obj.getString("horaIni");
                                horaFin = obj.getString("horaFin");
                                textoFinal=horariosLaboratorio.getText().toString();
                                horariosLaboratorio.setText( textoFinal + "\n" +
                                        getString(R.string.TitulacionJSON) + titulacion + "\n" +
                                                getString(R.string.codAsignaturaJSON) + codAsignatura + "\n" +
                                                getString(R.string.AsignaturaJSON) + asignatura + "\n" +
                                                getString(R.string.GrupoJSON) + grupo + "\n" +
                                                getString(R.string.DiaJSON) + dia + "\n" +
                                                getString(R.string.TextoHORAINITutoriasJSON) + horaIni + "\n" +
                                                getString(R.string.TextoHORAFINTutoriasJSON) + horaFin + "\n");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(LOGTAG, "dentro de onErrorResponse");
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq);

        horariosLaboratorio.setMovementMethod(new ScrollingMovementMethod());

    }


}
