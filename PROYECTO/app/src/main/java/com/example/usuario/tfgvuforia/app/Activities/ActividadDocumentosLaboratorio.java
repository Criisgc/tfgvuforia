package com.example.usuario.tfgvuforia.app.Activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.usuario.ConexionBBDD.AppController;
import com.example.usuario.ExpandibleBorrar;
import com.example.usuario.ExpandibleSimple;
import com.example.usuario.tfgvuforia.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cgc on 13/06/2018.
 */

public class ActividadDocumentosLaboratorio extends Activity {

    private static final String LOGTAG = "DocLaboActivity";

    //LAYOUT
    private TextView tituloDocumentos;
    private ExpandableListView expandableListView;

    private ExpandibleSimple adapter;
    private ArrayList<String> list;
    private Map<String, ArrayList<String>> mapChild;

    private String Laboratorio;
    private String url;

    //CONFIGURACION
    private String ip;

    //BBDD
    private JSONArray data;
    private String documento;
    private String titulo;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.documentoslaboratoriolayout);

        final Typeface font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");

        tituloDocumentos = (TextView) findViewById(R.id.titulodocumentoslab);
        tituloDocumentos.setTypeface(font);

        Laboratorio = ((AppController) this.getApplicationContext()).getDestinatario();
        //CONTENIDO DE CONFIGURACION
        ip=((AppController) this.getApplicationContext()).getIp();
        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=" + Laboratorio + "&pagina=0";

        expandableListView = (ExpandableListView) findViewById(R.id.textoDocumentoslab);
        list = new ArrayList<>();
        mapChild = new HashMap<>();

        getLaboratorio();
    }

    public void getLaboratorio() {

        //MOCKUP
        /*************/
        if(((AppController) this.getApplicationContext()).getMockup()){
            if(Laboratorio.equals("Informática")){
                for (int i = 0; i < 2; i++) {
                    if(i==0) {
                        titulo= "Guiones Informática Industrial";
                        documento = "http://www.elai.upm.es/moodle/course/view.php?id=59";
                    }
                    if(i==1) {
                        titulo= "Guiones Sistemas Informáticos Industriales";
                        documento = "https://bitbucket.org/arodrifi/sii_c1718/wiki/Enunciados_Curso_2017-2018";
                    }

                    ArrayList<String> lista = new ArrayList<>();
                    list.add(titulo);
                    lista.add(documento);

                    mapChild.put(list.get(i), lista);
                }
                adapter = new ExpandibleSimple(getApplicationContext(), list, mapChild, 1);
                expandableListView.setAdapter(adapter);
            }
            if(Laboratorio.equals("Electrónica")){
                for (int i = 0; i < 1; i++) {
                    if(i==0) {
                        titulo= "Guia uso del osciloscopio";
                        documento = "https://www.electronicafacil.net/tutoriales/Uso-del-osciloscopio.php";
                    }

                    ArrayList<String> lista = new ArrayList<>();
                    list.add(titulo);
                    lista.add(documento);

                    mapChild.put(list.get(i), lista);
                }
                adapter = new ExpandibleSimple(getApplicationContext(), list, mapChild, 1);
                expandableListView.setAdapter(adapter);
            }
        }
        /*************/

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(LOGTAG, "dentro de onResponse, url:" + url);
                        try {
                            data = response.getJSONArray("content");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject obj = data.getJSONObject(i);
                                titulo = obj.getString("titulo");
                                documento = obj.getString("documento");

                                ArrayList<String> lista = new ArrayList<>();
                                list.add(titulo);
                                lista.add(documento);

                                mapChild.put(list.get(i), lista);
                            }
                            adapter = new ExpandibleSimple(getApplicationContext(), list, mapChild, 1);
                            expandableListView.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(LOGTAG, "dentro de onErrorResponse");
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq);

    }


}
