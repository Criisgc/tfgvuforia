package com.example.usuario.tfgvuforia.app.Activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ExpandableListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.usuario.ConexionBBDD.AppController;
import com.example.usuario.ExpandibleBorrar;
import com.example.usuario.ExpandibleSimple;
import com.example.usuario.tfgvuforia.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cgc on 08/03/2018.
 */

public class ActividadAvisosDespacho extends Activity {

    private static final String LOGTAG = "avisosActivity";

    //LAYOUT
    private TextView tituloavisos;
    private TextView tituloProfesores;
    private RadioButton Botonprofesor1;
    private RadioButton Botonprofesor2;
    private ExpandableListView expandableListView;

    private ExpandibleSimple adapter;
    private ArrayList<String> list;
    private Map<String, ArrayList<String>> mapChild;

    private String Profesor;
    private String url;

    //BBDD
    private JSONArray data;
    private String nombre;
    private String aviso;

    //CONFIGURACION
    private String ip;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.avisosdespacholayout);

        final Typeface font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");

        tituloavisos = (TextView) findViewById(R.id.titulomostraravisos);
        tituloavisos.setTypeface(font);

        Profesor=((AppController) this.getApplicationContext()).getDestinatario();;

        //CONTENIDO DE CONFIGURACION
        ip=((AppController) this.getApplicationContext()).getIp();
        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=" + Profesor + "&pagina=0";

        expandableListView = (ExpandableListView) findViewById(R.id.textoAvisosDesp);
        list = new ArrayList<>();
        mapChild = new HashMap<>();

        getAvisos();
    }

    public void getAvisos() {

        //MOCKUP
        /*************/
        if(((AppController) this.getApplicationContext()).getMockup()){
            if(Profesor.equals("Maria Esther Palacios Lorenzo")){
                for (int i = 0; i < 2; i++) {
                    if(i==0) {
                        aviso = "No voy a poder estar en el despacho hasta la 1 de la tarde hoy.";
                    }
                    if(i==1) {
                        aviso = "Hasta el día 15 de septiembre no se respetara el horario de tutorias fijado. Para fijar una fecha de contacto mandar un email o un mensaje a través de la app Aumenti";
                    }

                    ArrayList<String> lista = new ArrayList<>();
                    list.add("Aviso " + (i + 1));
                    lista.add(aviso);

                    mapChild.put(list.get(i), lista);
                }
                adapter = new ExpandibleSimple(getApplicationContext(), list, mapChild, 0);
                expandableListView.setAdapter(adapter);
            }
        }
        /*************/
        else{
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(LOGTAG, "dentro de onResponse, url:" + url);
                            try {
                                data = response.getJSONArray("content");
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject obj = data.getJSONObject(i);
                                    aviso = obj.getString("aviso");

                                    ArrayList<String> lista = new ArrayList<>();
                                    list.add("Aviso " + (i + 1));
                                    lista.add(aviso);

                                    mapChild.put(list.get(i), lista);
                                }
                                adapter = new ExpandibleSimple(getApplicationContext(), list, mapChild, 0);
                                expandableListView.setAdapter(adapter);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(LOGTAG, "dentro de onErrorResponse");
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
            };
            AppController.getInstance().addToRequestQueue(jsonObjReq);
        }

    }

}
