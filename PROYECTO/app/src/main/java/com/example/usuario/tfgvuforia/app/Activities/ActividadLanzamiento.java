package com.example.usuario.tfgvuforia.app.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.usuario.ConexionBBDD.AppController;
import com.example.usuario.tfgvuforia.R;

/**
 * Created by cgc on 26/01/2018.
 */

public class ActividadLanzamiento extends Activity{

    private static long SPLASH_MILLIS = 1000;
    private TextView titulo;

    private SharedPreferences sharedPref;
    private String ip;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        final Typeface font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");

        LayoutInflater inflater = LayoutInflater.from(this);

        RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.lanzamientolayout, null, false);
        addContentView(layout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        titulo = findViewById(R.id.textView3);
        titulo.setTypeface(font);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        ip = sharedPref.getString("Ip", "DEFAULT");
        ((AppController) getApplication()).setIp(ip);


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                Intent intent = new Intent(ActividadLanzamiento.this, ActividadAutenticacion.class);
                startActivity(intent);
            }
        }, SPLASH_MILLIS);
    }
}


