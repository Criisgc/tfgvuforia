package com.example.usuario.tfgvuforia.app.Activities;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.usuario.tfgvuforia.R;

/**
 * Created by cgc on 21/02/2018.
 */

public class ActividadInformacion extends Activity {

    //LAYOUT
    private TextView tituloinformacion;
    private TextView informacion;
    private ImageView profesorTarget;
    private ImageView claseTarget;
    private ImageView laboratorioTarget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.informacionlayout);

        final Typeface font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");

        tituloinformacion = (TextView) findViewById(R.id.tituloInfor);
        tituloinformacion.setTypeface(font);
        informacion = (TextView) findViewById(R.id.textoinformacion);
        informacion.setTypeface(font);
        profesorTarget = (ImageView) findViewById(R.id.targetProfesor);
        claseTarget = (ImageView) findViewById(R.id.targetClase);
        laboratorioTarget = (ImageView) findViewById(R.id.targetLaboratorio);
    }

}
