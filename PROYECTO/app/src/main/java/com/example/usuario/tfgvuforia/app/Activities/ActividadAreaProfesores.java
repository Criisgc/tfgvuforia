package com.example.usuario.tfgvuforia.app.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.usuario.AlertDialogManager;
import com.example.usuario.ConexionBBDD.AppController;
import com.example.usuario.ExpandibleBorrar;
import com.example.usuario.ExpandibleBorrarResponder;
import com.example.usuario.tfgvuforia.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cgc on 07/03/2018.
 */

public class ActividadAreaProfesores extends Activity {

    private static final String LOGTAG = "areaProfesoresActivity";

    //LAYOUT
    private RadioButton Botoncampana;
    private RadioButton Botonmensaje;
    private RadioButton Botontablon;
    private TextView titulo;
    private TextView tituloTexto;
    private EditText mandaraviso;
    private Button enviaraviso;
    private RadioButton BotonDespacho;
    private RadioButton BotonClase;
    private RadioButton BotonLaboratorio;
    private Spinner spinnerDespacho;
    private Spinner spinnerClase;
    private Spinner spinnerLaboratorio;
    private ExpandableListView expandableListView;
    private RadioButton BotonOtroLugar;
    private EditText textoOtroLugar;

    private ExpandibleBorrar adapterEstancias;
    private ArrayList<String> listEstancias;
    private ArrayList<String> listMensajes;
    private Map<String, ArrayList<String>> mapChildEstancias;
    private Map<String, ArrayList<String>> mapChildMensajes;

    private String user;

    private Typeface font;

    //BBDD
    private String nombre;
    private String asunto;
    private String mensaje;
    private String hora;
    private JSONArray data;
    private String url;
    private String mensajeAviso;

    //CONFIGURACION
    private boolean prefNotificaciones;
    private SharedPreferences sharedPref;
    private String ip;

    private String direccion= "";
    private  ArrayAdapter<String> adapterDespacho;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        String[] valoresLaboratorio = {"", getString(R.string.LABFisica), getString(R.string.LABQuimica), getString(R.string.LABFabricacion), getString(R.string.LABElectronica), getString(R.string.LABInformatica), getString(R.string.LABCienciaMat), getString(R.string.LABMaqElec)};
        String[] valoresDespacho = {"", "C102", "C103", "C104", "C105", "C106", "C107", "C201", "C202", "C203", "C204", "C301",
                "C302", "C303", "C304", "C305"};
        String[] valoresClase = {"", "B11", "B12", "B21", "B22", "B31", "B32", "B41", "B42", "A10", "A11", "A12",
                "A13", "A14", "A15", "A16", "A21", "A22", "A23", "A24", "A25", "A26", "A27", "A28", "A34", "A35"};

        super.onCreate(savedInstanceState);
        setContentView(R.layout.areaprofesoreslayout);

        font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");

        spinnerDespacho = (Spinner) findViewById(R.id.SpinnerDespacho);
        adapterDespacho = new ArrayAdapter<String>(this, R.layout.especificacionesspinner, valoresDespacho) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTypeface(font);
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                ((TextView) v).setTypeface(font);
                return v;
            }
        };
        adapterDespacho.setDropDownViewResource(R.layout.especificacionesspinner);
        spinnerDespacho.setAdapter(adapterDespacho);


        spinnerClase = (Spinner) findViewById(R.id.SpinnerClase);
        ArrayAdapter<String> adapterClase = new ArrayAdapter<String>(this, R.layout.especificacionesspinner, valoresClase) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTypeface(font);
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                ((TextView) v).setTypeface(font);
                return v;
            }
        };

        adapterClase.setDropDownViewResource(R.layout.especificacionesspinner);
        spinnerClase.setAdapter(adapterClase);

        spinnerLaboratorio = (Spinner) findViewById(R.id.SpinnerLaboratorio);
        ArrayAdapter<String> adapterLaboratorio = new ArrayAdapter<String>(this, R.layout.especificacionesspinner, valoresLaboratorio) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTypeface(font);
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                ((TextView) v).setTypeface(font);
                return v;
            }
        };
        adapterLaboratorio.setDropDownViewResource(R.layout.especificacionesspinner);
        spinnerLaboratorio.setAdapter(adapterLaboratorio);

        titulo = (TextView) findViewById(R.id.tituloAreaProfesores);
        titulo.setTypeface(font);

        Botoncampana = (RadioButton) findViewById(R.id.avisosEst);
        Botonmensaje = (RadioButton) findViewById(R.id.avisosMens);
        Botontablon = (RadioButton) findViewById(R.id.EscAvis);

        BotonDespacho = (RadioButton) findViewById(R.id.avisoaDespacho);
        BotonDespacho.setTypeface(font);
        BotonClase = (RadioButton) findViewById(R.id.avisoaClase);
        BotonClase.setTypeface(font);
        BotonLaboratorio = (RadioButton) findViewById(R.id.avisoaLaboratorio);
        BotonLaboratorio.setTypeface(font);
        BotonOtroLugar = (RadioButton) findViewById(R.id.avisoaOtro);
        BotonOtroLugar.setTypeface(font);
        textoOtroLugar = (EditText) findViewById(R.id.textoOtroLugar);
        textoOtroLugar.setTypeface(font);
        mandaraviso = (EditText) findViewById(R.id.textoAviso);
        mandaraviso.setTypeface(font);
        tituloTexto = (TextView) findViewById(R.id.tituloTexto);
        tituloTexto.setTypeface(font);
        enviaraviso = (Button) findViewById(R.id.buttonEnviarAviso);
        enviaraviso.setTypeface(font);

        user = getIntent().getExtras().getString("Usuario");

        //CONTENIDO DE CONFIGURACION
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        prefNotificaciones = sharedPref.getBoolean("Notificaciones", false);
        ip=((AppController) this.getApplicationContext()).getIp();

        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=" + user;

        expandableListView = (ExpandableListView) findViewById(R.id.textoAreaProfesores);
        listEstancias = new ArrayList<>();
        mapChildEstancias = new HashMap<>();
        listMensajes = new ArrayList<>();
        mapChildMensajes = new HashMap<>();

        getEstancias();
    }

    public void onRadioButtonClicked(View view) {

        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.avisosEst: {
                tituloTexto.setText(R.string.TituloTextoAVISOAreaProfesores);
                mandaraviso.setVisibility(View.INVISIBLE);
                enviaraviso.setVisibility(View.INVISIBLE);
                BotonDespacho.setVisibility(View.INVISIBLE);
                BotonClase.setVisibility(View.INVISIBLE);
                BotonLaboratorio.setVisibility(View.INVISIBLE);
                BotonOtroLugar.setVisibility(View.INVISIBLE);
                textoOtroLugar.setVisibility(View.INVISIBLE);
                spinnerDespacho.setVisibility(View.INVISIBLE);
                spinnerClase.setVisibility(View.INVISIBLE);
                spinnerLaboratorio.setVisibility(View.INVISIBLE);
                expandableListView.setVisibility(View.VISIBLE);
                getEstancias();
                break;
            }
            case R.id.avisosMens: {
                tituloTexto.setText(R.string.TituloMensajeLayoutAreaProfesores);
                mandaraviso.setVisibility(View.INVISIBLE);
                enviaraviso.setVisibility(View.INVISIBLE);
                BotonDespacho.setVisibility(View.INVISIBLE);
                BotonClase.setVisibility(View.INVISIBLE);
                BotonLaboratorio.setVisibility(View.INVISIBLE);
                BotonOtroLugar.setVisibility(View.INVISIBLE);
                textoOtroLugar.setVisibility(View.INVISIBLE);
                spinnerDespacho.setVisibility(View.INVISIBLE);
                spinnerClase.setVisibility(View.INVISIBLE);
                spinnerLaboratorio.setVisibility(View.INVISIBLE);
                expandableListView.setVisibility(View.VISIBLE);
                getMensajes();
                break;
            }
            case R.id.EscAvis: {
                tituloTexto.setText(R.string.TituloEscribirAvisoLayoutAreaProfesores);
                mandaraviso.setVisibility(View.VISIBLE);
                enviaraviso.setVisibility(View.VISIBLE);
                BotonDespacho.setVisibility(View.VISIBLE);
                BotonClase.setVisibility(View.VISIBLE);
                BotonLaboratorio.setVisibility(View.VISIBLE);
                BotonOtroLugar.setVisibility(View.VISIBLE);
                textoOtroLugar.setVisibility(View.VISIBLE);
                spinnerDespacho.setVisibility(View.VISIBLE);
                spinnerClase.setVisibility(View.VISIBLE);
                spinnerLaboratorio.setVisibility(View.VISIBLE);
                expandableListView.setVisibility(View.INVISIBLE);

                mensajeAviso = mandaraviso.getText().toString();
                enviaraviso.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String direccion=setDireccion(ActividadAreaProfesores.this);

                        if (direccion == "") {
                            Toast toast1 = new Toast(getApplicationContext());
                            LayoutInflater inflater= getLayoutInflater();
                            View layout= inflater.inflate(R.layout.toastlayout, null);
                            TextView txtmsg = (TextView) layout.findViewById(R.id.textoToast);
                            txtmsg.setText(R.string.ERRORdirEnvio);
                            txtmsg.setTypeface(font);
                            toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                            toast1.setView(layout);
                            toast1.show();
                        } else {
                            AlertDialogManager alertDialogManager = new AlertDialogManager();
                            alertDialogManager.showAlertDialogAviso(ActividadAreaProfesores.this);
                        }
                    }
                });
                break;
            }

        }
    }

    public void getMensajes() {

        listMensajes.clear();

        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=";

        //MOCKUP
        /**************/
        if(((AppController) this.getApplicationContext()).getMockup()){
            if(user.contains("pcgc@upm.es")){
                for (int i = 0; i < 4; i++) {
                    if(i==0){
                        nombre = "Juan López";
                        asunto = "Tutoría";
                        mensaje = "hola, queria saber cuando encontrarte en el despacho para poder quedar y hablar sobre el tfg de cuando presentarlo y de como estructurar la memoria ya que tengo algunas dudas. Muchas gracias" +
                                "Un saludo";
                    }
                    if(i==1){
                        nombre = "María Marquez";
                        asunto = "Laboratorio";
                        mensaje = "Se puede cambiar el grupo de laboratorio asignado?";
                    }
                    if(i==2){
                        nombre = "Silvia Sanchez";
                        asunto = "problemas clase";
                        mensaje = "puedes subir a moodle los problemaas que se hicieron en clase el otro día? Gracias";
                    }
                    if(i==3){
                        nombre = "David Martín";
                        asunto = "Fecha examen";
                        mensaje = "Cual es la fecha del primer examen de fisica 1?. Gracias";
                    }

                    ArrayList<String> lista = new ArrayList<>();
                    listMensajes.add(nombre + "\n" + asunto);
                    lista.add(mensaje);
                    mapChildMensajes.put(listMensajes.get(i), lista);
                }
                final ExpandibleBorrarResponder adapterMensajes = new ExpandibleBorrarResponder(getApplicationContext(), listMensajes, mapChildMensajes, ActividadAreaProfesores.this, 0);
                expandableListView.setAdapter(adapterMensajes);
            }
            else if(user.contains("pvgm@upm.es")){
                for (int i = 0; i < 2; i++) {
                    if(i==0){
                        nombre = "Marta Perez";
                        asunto = "Tutoría";
                        mensaje = "Hola, cuando podemos vernos antes del comienzo de las clases? Un saludo.";
                    }
                    if(i==1){
                        nombre = "Alejandro Bermejo";
                        asunto = "Clases";
                        mensaje = "Me es imposible acudir a clase las 2 primeras semanas. Hay algun inconveniente en que falte? Gracias";
                    }

                    ArrayList<String> lista = new ArrayList<>();
                    listMensajes.add(nombre + "\n" + asunto);
                    lista.add(mensaje);
                    mapChildMensajes.put(listMensajes.get(i), lista);
                }
                final ExpandibleBorrarResponder adapterMensajes = new ExpandibleBorrarResponder(getApplicationContext(), listMensajes, mapChildMensajes, ActividadAreaProfesores.this, 0);
                expandableListView.setAdapter(adapterMensajes);
            }

        }
        /**************/

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(LOGTAG, "dentro de onResponse, url:" + url);

                        try {
                            data = response.getJSONArray("content");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject obj = data.getJSONObject(i);
                                nombre = obj.getString("nombre");
                                mensaje = obj.getString("mensaje");

                                ArrayList<String> lista = new ArrayList<>();
                                listMensajes.add(nombre);
                                lista.add(mensaje);

                                mapChildMensajes.put(listMensajes.get(i), lista);
                            }
                            final ExpandibleBorrarResponder adapterMensajes = new ExpandibleBorrarResponder(getApplicationContext(), listMensajes, mapChildMensajes, ActividadAreaProfesores.this, 0);
                            expandableListView.setAdapter(adapterMensajes);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(LOGTAG, "dentro de onErrorResponse getMensajes");
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq);
        mandaraviso.setMovementMethod(new ScrollingMovementMethod());
    }

    public void getEstancias() {

        listEstancias.clear();

        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=";

        //MOCKUP
        /**************/
        if(((AppController) this.getApplicationContext()).getMockup()){
            if(user.contains("pcgc@upm.es")){
                for (int i = 0; i < 2; i++) {
                    if(i==0) {
                        nombre = "Cristina García";
                        hora = "Lunes 4 Septiembre 10:58:08";
                    }
                    if(i==1) {
                        nombre = "Pedro Muñoz";
                        hora = "Lunes 4 Septiembre 11:32:27";
                    }


                    ArrayList<String> lista = new ArrayList<>();
                    listEstancias.add(nombre);
                    lista.add(hora);

                    mapChildEstancias.put(listEstancias.get(i), lista);
                }
                adapterEstancias = new ExpandibleBorrar(getApplicationContext(), listEstancias, mapChildEstancias);
                expandableListView.setAdapter(adapterEstancias);
            }
            else if(user.contains("pvgm@upm.es")){
                for (int i = 0; i < 1; i++) {
                    if(i==0) {
                        nombre = "Luis Sanchez";
                        hora = "Lunes 11 Septiembre 12:53:06";
                    }

                    ArrayList<String> lista = new ArrayList<>();
                    listEstancias.add(nombre);
                    lista.add(hora);

                    mapChildEstancias.put(listEstancias.get(i), lista);
                }
                adapterEstancias = new ExpandibleBorrar(getApplicationContext(), listEstancias, mapChildEstancias);
                expandableListView.setAdapter(adapterEstancias);
            }

        }
        /**************/

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(LOGTAG, "dentro de onResponse, url:" + url);

                        try {
                            data = response.getJSONArray("content");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject obj = data.getJSONObject(i);
                                nombre = obj.getString("nombre");
                                hora = obj.getString("hora");

                                ArrayList<String> lista = new ArrayList<>();
                                listEstancias.add(nombre);
                                lista.add(hora);

                                mapChildEstancias.put(listEstancias.get(i), lista);
                            }
                            adapterEstancias = new ExpandibleBorrar(getApplicationContext(), listEstancias, mapChildEstancias);
                            expandableListView.setAdapter(adapterEstancias);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(LOGTAG, "dentro de onErrorResponse getEstancias");
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq);
        mandaraviso.setMovementMethod(new ScrollingMovementMethod());
    }


   public String setDireccion(Context context){
       boolean clasePulsada;
       boolean despachopulsado;
       boolean laboratoriopulsado;
       boolean otrolugarpulsado;

       clasePulsada = BotonClase.isChecked();
       despachopulsado = BotonDespacho.isChecked();
       laboratoriopulsado = BotonLaboratorio.isChecked();
       otrolugarpulsado = BotonOtroLugar.isChecked();

       if (clasePulsada) {
           direccion = spinnerClase.getSelectedItem().toString();
       } else if (despachopulsado) {
           direccion = spinnerDespacho.getSelectedItem().toString();
       } else if (laboratoriopulsado) {
           direccion = spinnerLaboratorio.getSelectedItem().toString();
       } else if (otrolugarpulsado){
           direccion = textoOtroLugar.toString();
       }
       return direccion;
   }


    public boolean putAviso(final Context context) {

        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=";

        Map<String, String> params = new HashMap<String, String>();
            params.put("User", user);
            params.put("Aviso", mensajeAviso);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if(!((AppController) context.getApplicationContext()).getMockup()) {
                                Log.d("PRA", "1");
                                Toast toast1 = new Toast(context);
                                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View layout = inflater.inflate(R.layout.toastlayout, null);
                                TextView txtmsg = (TextView) layout.findViewById(R.id.textoToast);
                                txtmsg.setText(R.string.avisoCorrecto);
                                toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                                toast1.setView(layout);
                                toast1.show();
                            }
                            //MOCKUP
                            /*********/
                           else if(((AppController) context.getApplicationContext()).getMockup()){
                                Log.d("PRA", "2");
                                Toast toast2 = new Toast(context);
                                LayoutInflater inflater2= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View layout2= inflater2.inflate(R.layout.toastlayout, null);
                                TextView txtmsg2 = (TextView) layout2.findViewById(R.id.textoToast);
                                txtmsg2.setText(R.string.AvisoINCORRECTO);
                                txtmsg2.setTypeface(font);
                                toast2.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                                toast2.setView(layout2);
                                toast2.show();
                            }
                            /*****/
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(LOGTAG, "dentro de onErrorResponse" + error.getMessage());
                    if(!((AppController) context.getApplicationContext()).getMockup()) {
                        Log.d("PRA", "3");
                        Toast toast1 = new Toast(context);
                        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout = inflater.inflate(R.layout.toastlayout, null);
                        TextView txtmsg = (TextView) layout.findViewById(R.id.textoToast);
                        txtmsg.setText(R.string.AvisoINCORRECTO);
                        txtmsg.setTypeface(font);
                        toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast1.setView(layout);
                        toast1.show();
                    }
                    //MOCKUP
                    /*********/
                    else if(((AppController) context.getApplicationContext()).getMockup()){
                        Log.d("PRA", "4");
                        Toast toast2 = new Toast(context);
                        LayoutInflater inflater2= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout2= inflater2.inflate(R.layout.toastlayout, null);
                        TextView txtmsg2 = (TextView) layout2.findViewById(R.id.textoToast);
                        txtmsg2.setText(R.string.avisoCorrecto);
                        toast2.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast2.setView(layout2);
                        toast2.show();
                    }
                    /*****/
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json; charset=UTF-8");
                    return params;
                }
            };
            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(request);
        return true;
    }

    public void deleteAlert(final Context context){

        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=";

        Map<String,String> params = new HashMap<String, String>();
        params.put("User", user);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout= inflater.inflate(R.layout.toastlayout, null);
                        TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
                        txtmsg.setText(R.string.AlertaBorrada);
                        txtmsg.setTypeface(font);
                        Toast toast1 = new Toast(context);
                        toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                        toast1.setView(layout);
                        toast1.show();
                        toast1.setDuration(Toast.LENGTH_SHORT);

                        getEstancias();

                        //MOCKUP
                        /*********/
                        if(((AppController) context).getMockup()){
                            LayoutInflater inflater2=  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View layout2= inflater.inflate(R.layout.toastlayout, null);
                            TextView txtmsg2= (TextView) layout.findViewById(R.id.textoToast);
                            txtmsg2.setText(R.string.ERRORalertaBorrada);
                            txtmsg2.setTypeface(font);
                            Toast toast2 = new Toast(context);
                            toast2.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                            toast2.setView(layout);
                            toast2.show();
                            toast2.setDuration(Toast.LENGTH_SHORT);
                        }
                        /*****/
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(LOGTAG, "dentro de onErrorResponse" + error.getMessage());

                LayoutInflater inflater=  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout= inflater.inflate(R.layout.toastlayout, null);
                TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
                txtmsg.setText(R.string.ERRORalertaBorrada);
                txtmsg.setTypeface(font);
                Toast toast1 = new Toast(context);
                toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                toast1.setView(layout);
                toast1.show();
                toast1.setDuration(Toast.LENGTH_SHORT);

                //MOCKUP
                /*********/
                if(((AppController) context).getMockup()){
                    LayoutInflater inflater2= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout2= inflater.inflate(R.layout.toastlayout, null);
                    TextView txtmsg2= (TextView) layout.findViewById(R.id.textoToast);
                    txtmsg2.setText(R.string.AlertaBorrada);
                    txtmsg2.setTypeface(font);
                    Toast toast2 = new Toast(context);
                    toast2.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                    toast2.setView(layout);
                    toast2.show();
                    toast2.setDuration(Toast.LENGTH_SHORT);
                }
                /*****/

            }
        }) {
            @Override
            public Map<String,String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                return params;
            }
        };
        RequestQueue queue= Volley.newRequestQueue(context);
        queue.add(request);
    }

    public void deleteMessage(final Context context){

        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=";

        Map<String,String> params = new HashMap<String, String>();
        params.put("User", user);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast toast1 = new Toast(context);
                        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout= inflater.inflate(R.layout.toastlayout, null);
                        TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
                        txtmsg.setText(R.string.MensajeBorrado);
                        txtmsg.setTypeface(font);

                        toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                        toast1.setView(layout);
                        toast1.show();
                        toast1.setDuration(Toast.LENGTH_SHORT);

                        getMensajes();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(LOGTAG, "dentro de onErrorResponse" + error.getMessage());
                Toast toast1 = new Toast(context);
                LayoutInflater inflater=  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout= inflater.inflate(R.layout.toastlayout, null);
                TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
                txtmsg.setText(R.string.ERRORmensajeBorrado);
                txtmsg.setTypeface(font);

                toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                toast1.setView(layout);
                toast1.show();
                toast1.setDuration(Toast.LENGTH_SHORT);

                //MOCKUP
                /*********/
                if(((AppController) context).getMockup()){
                    Toast toast2 = new Toast(context);
                    LayoutInflater inflater2= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout2= inflater.inflate(R.layout.toastlayout, null);
                    TextView txtmsg2= (TextView) layout.findViewById(R.id.textoToast);
                    txtmsg.setText(R.string.MensajeBorrado);
                    txtmsg.setTypeface(font);

                    toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                    toast1.setView(layout);
                    toast1.show();
                    toast1.setDuration(Toast.LENGTH_SHORT);
                }
                /*****/
            }
        }) {
            @Override
            public Map<String,String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                return params;
            }
        };
        RequestQueue queue= Volley.newRequestQueue(context);
        queue.add(request);
    }
}
