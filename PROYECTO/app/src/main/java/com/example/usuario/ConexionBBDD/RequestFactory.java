package com.example.usuario.ConexionBBDD;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

/**
 * Created by cgc on 12/06/2018.
 */

public interface RequestFactory {

    public StringRequest getRequest(Response.Listener<String> responseListener,
                                    Response.ErrorListener errorListener);

}