package com.example.usuario.tfgvuforia.app.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.preference.PreferenceActivity;
import android.os.Bundle;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.usuario.ConexionBBDD.AppController;
import com.example.usuario.tfgvuforia.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by cgc on 21/02/2018.
 */
public class ActividadConfiguracion extends PreferenceActivity  {

    private static final String LOGTAG = "settingActivity";

    private String idioma;
    private SharedPreferences sharedPref;
    private int role;
    private Locale locale;
    private Configuration config= new Configuration();
    private SharedPreferences.OnSharedPreferenceChangeListener listener;
    private Typeface font;
    private String url;
    private String ip;
    private Context context;

    private View tryInflate(String name, Context context, AttributeSet attrs) {
        LayoutInflater li = LayoutInflater.from(context);
        View v = null;
        try {
            v = li.createView(name, null, attrs);
        } catch (Exception e) {
            try {
                v = li.createView("android.widget." + name, null, attrs);
            } catch (Exception e1) {
            }
        }
        return v;
    }
    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.setting);

        context=getApplicationContext();

        role= getIntent().getExtras().getInt("Role");

        if(role==0) {
            PreferenceCategory category = (PreferenceCategory) findPreference("Categoria");
            category.removePreference(findPreference("RecibirAvisos"));
        }

        font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");
        getLayoutInflater().setFactory(new LayoutInflater.Factory() {
            @Override
            public View onCreateView(String name, Context context,
                                     AttributeSet attrs) {
                View v = tryInflate(name, context, attrs);
                if (v instanceof TextView) {
                    ((TextView) v).setTypeface(font);
                }
                return v;
            }
        });

        sharedPref= PreferenceManager.getDefaultSharedPreferences(this);

        listener= new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
                if(s.equals("Idioma"))
                     setIdioma();
                if(s.equals("RecibirAvisos"))
                    putAvisos();
            }
        };
        sharedPref.registerOnSharedPreferenceChangeListener(listener);
    }


    public void setIdioma(){
        idioma=sharedPref.getString("Idioma", "DEFAULT");
        switch(idioma){
            case "es": {
               locale= new Locale("es");
               config.locale = locale;

                break;
            }
            case "en":{
                locale = new Locale("en");
                config= new Configuration();
                config.locale = locale;

                break;
                }
        }
        getResources().updateConfiguration(config, null);

        LayoutInflater inflater= getLayoutInflater();
        View layout= inflater.inflate(R.layout.toastlayout, (ViewGroup) findViewById(R.id.toastlayout));
        TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
        txtmsg.setText(R.string.AplicacionCambioIdioma);
        txtmsg.setTypeface(font);
        Toast toast1 = new Toast(getApplicationContext());
        toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
        toast1.setView(layout);
        toast1.show();
        toast1.setDuration(Toast.LENGTH_SHORT);


        }

        public void putAvisos()
        {
            ip=sharedPref.getString("ip", "DEFAULT");
            url="http://" + ip + ":8081/auth/login";
            Map<String,String> params = new HashMap<String, String>();
            params.put("Avisos", "Sí");

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if(!((AppController) context.getApplicationContext()).getMockup()){
                                LayoutInflater inflater= (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View layout= inflater.inflate(R.layout.toastlayout, null);
                                TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
                                txtmsg.setText(R.string.CambiosOk);
                                txtmsg.setTypeface(font);
                                Toast toast1 = new Toast(getApplicationContext());
                                toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                                toast1.setView(layout);
                                toast1.show();
                                toast1.setDuration(Toast.LENGTH_SHORT);
                            }
                            else{
                                LayoutInflater inflater= (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View layout= inflater.inflate(R.layout.toastlayout, null);
                                TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
                                txtmsg.setText(R.string.ERRORaplCambios);
                                txtmsg.setTypeface(font);
                                Toast toast1 = new Toast(getApplicationContext());
                                toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                                toast1.setView(layout);
                                toast1.show();
                                toast1.setDuration(Toast.LENGTH_SHORT);
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(LOGTAG, "dentro de onErrorResponse" + error.getMessage());
                    if(!((AppController) context.getApplicationContext()).getMockup()){
                        LayoutInflater inflater= (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout= inflater.inflate(R.layout.toastlayout, null);
                        TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
                        txtmsg.setText(R.string.ERRORaplCambios);
                        txtmsg.setTypeface(font);
                        Toast toast1 = new Toast(getApplicationContext());
                        toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                        toast1.setView(layout);
                        toast1.show();
                        toast1.setDuration(Toast.LENGTH_SHORT);
                    }
                    else{
                        LayoutInflater inflater= (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout= inflater.inflate(R.layout.toastlayout, null);
                        TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
                        txtmsg.setText(R.string.CambiosOk);
                        txtmsg.setTypeface(font);
                        Toast toast1 = new Toast(getApplicationContext());
                        toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                        toast1.setView(layout);
                        toast1.show();
                        toast1.setDuration(Toast.LENGTH_SHORT);
                    }

                }
            }) {
                @Override
                public Map<String,String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json; charset=UTF-8");
                    return params;
                }
            };
            RequestQueue queue= Volley.newRequestQueue(getApplicationContext());
            queue.add(request);
        }

}
