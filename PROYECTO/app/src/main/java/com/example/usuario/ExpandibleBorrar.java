package com.example.usuario;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.usuario.tfgvuforia.R;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by cgc on 10/07/2018.
 */

public class ExpandibleBorrar extends BaseExpandableListAdapter {

    private ArrayList<String> listTitulo;
    private Map<String, ArrayList<String>> mapChild;
    private Context context;

    public ExpandibleBorrar(Context context, ArrayList<String> listTitulo, Map<String, ArrayList<String>> mapChild){
        this.context=context;
        this.listTitulo=listTitulo;
        this.mapChild=mapChild;
    }

    @Override
    public int getGroupCount() {

        return listTitulo.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return mapChild.get(listTitulo.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return listTitulo.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return mapChild.get(listTitulo.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {

        final Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/futura.ttf");

        String Nombre = (String) getGroup(i);
        view= LayoutInflater.from(context).inflate(R.layout.expandablegroup, null);
        TextView textViewGroup = (TextView) view.findViewById(R.id.ExpanGroup);
        textViewGroup.setText(Nombre);
        textViewGroup.setTypeface(font);
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        final Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/futura.ttf");

        String mensaje = (String) getChild(i,i1);
        view= LayoutInflater.from(context).inflate(R.layout.expandablechildmessage, null);
        TextView textViewChild = view.findViewById(R.id.ExpanChild);
        ImageButton deleteButton = view.findViewById(R.id.ButtonDeleteAviso);
        textViewChild.setText(mensaje);
        textViewChild.setTypeface(font);

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialogManager alertDialogManager= new AlertDialogManager();
                alertDialogManager.showAlertDialogDeleteAlert(context);
            }
        });

        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
