package com.example.usuario.Authenticator;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by cgc on 12/06/2018.
 */

public class AuthenticatorService extends Service {

    private static final String TAG = "AuthenticationService";

    @Override
    public IBinder onBind(Intent intent){
        AccountAuthenticator authenticator = new AccountAuthenticator(this);
        return authenticator.getIBinder();
    }
}
