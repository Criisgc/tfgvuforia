/*===============================================================================
Copyright (c) 2016 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package com.example.usuario.tfgvuforia.app.Activities;

import java.io.IOException;
import java.util.Vector;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.content.res.AssetManager;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import com.example.usuario.ConexionBBDD.AppController;
import com.example.usuario.vuforia.utiles.Documentos;
import com.example.usuario.vuforia.utiles.ObjetoShaders;
import com.example.usuario.vuforia.utiles.Mensajes;
import com.example.usuario.vuforia.utiles.Horarios;
import com.example.usuario.vuforia.utiles.Avisos;
import com.example.usuario.vuforia.utiles.Campana;

import com.example.usuario.vuforia.utiles.Textura;
import com.example.usuario.vuforia.utiles.Utiles;
import com.vuforia.Device;
import com.vuforia.Matrix34F;
import com.vuforia.Matrix44F;
import com.vuforia.State;
import com.vuforia.Tool;
import com.vuforia.Trackable;
import com.vuforia.TrackableResult;
import com.vuforia.Vuforia;
import com.example.usuario.vuforia.SampleAppRenderer;
import com.example.usuario.vuforia.SampleAppRendererControl;
import com.example.usuario.vuforia.SampleApplicationSession;
import com.example.usuario.vuforia.utiles.GestorDiag;


// The renderer class for the ActividadCamaraPrincipal sample.
public class Renderizador extends Activity implements GLSurfaceView.Renderer, SampleAppRendererControl
{
    private static final String LOGTAG = "Renderizador";

    public static int vertexHandle;
    public static int mvpMatrixHandle;
    public static int textureCoordHandle;
    public static int texSampler2DHandle;
    /* Asset para obtener cada uno de los ficheros de configuracion                                         */
    public static AssetManager asset;

    private SampleApplicationSession vuforiaAppSession;
    private ActividadCamaraPrincipal mActivity;
    private SampleAppRenderer mSampleAppRenderer;

    private Vector<Textura> mTextures;
    
    private int shaderProgramID;

    private float modelescale=1.0f;

    private Mensajes mensajes;
    private Horarios horarios;
    private Avisos avisos;
    private Campana campana;
    private Documentos documentos;

    private boolean mIsActive = false;
    private boolean mModelIsLoaded = false;

    private boolean avisosActivados;
    private int touch;
    private int numTarget;
    private int typetarget;

    /*Constructor*/
    public Renderizador(ActividadCamaraPrincipal activity, SampleApplicationSession session)
    {
        mActivity = activity;
        vuforiaAppSession = session;
        // SampleAppRenderer used to encapsulate the use of RenderingPrimitives setting the device mode AR/VR and stereo mode
        mSampleAppRenderer = new SampleAppRenderer(this, mActivity, Device.MODE.MODE_AR, false, 0.01f , 5f);

        mensajes = new Mensajes();
        horarios = new Horarios();
        avisos =new Avisos();
        campana=new Campana();
        documentos= new Documentos();
    }

    // Called to draw the current frame.
    @Override
    public void onDrawFrame(GL10 gl)
    {
        if (!mIsActive)
            return;
        // Call our function to render content from SampleAppRenderer class
        mSampleAppRenderer.render();
    }
    

    public void setActive(boolean active)
    {
        mIsActive = active;
        if(mIsActive)
            mSampleAppRenderer.configureVideoBackground();
    }

    // Called when the surface is created or recreated.
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        //Call Vuforia function to (re)initialize rendering after first use or after OpenGL ES context was lost (e.g. after onPause/onResume):
        vuforiaAppSession.onSurfaceCreated();
        mSampleAppRenderer.onSurfaceCreated();
        mActivity.hideCard();
    }

    // Called when the surface changed size.
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        // Call Vuforia function to handle render surface size changes:
        vuforiaAppSession.onSurfaceChanged(width, height);
        // RenderingPrimitives to be updated when some rendering change is done
        mSampleAppRenderer.onConfigurationChanged(mIsActive);
        initRendering();
    }

    // Function for initializing the renderer.
    private void initRendering()
    {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, Vuforia.requiresAlpha() ? 0.0f : 1.0f);
        for (Textura t : mTextures)
        {
            GLES20.glGenTextures(1, t.mTextureID, 0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, t.mTextureID[0]);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, t.mWidth, t.mHeight, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, t.mData);
        }

        shaderProgramID = Utiles.createProgramFromShaderSrc(
                ObjetoShaders.OBJECT_MESH_VERTEX_SHADER,
                ObjetoShaders.OBJECT_MESH_FRAGMENT_SHADER);

        vertexHandle = GLES20.glGetAttribLocation(shaderProgramID, "vertexPosition");
        textureCoordHandle = GLES20.glGetAttribLocation(shaderProgramID, "vertexTexCoord");
        mvpMatrixHandle = GLES20.glGetUniformLocation(shaderProgramID, "modelViewProjectionMatrix");
        texSampler2DHandle = GLES20.glGetUniformLocation(shaderProgramID, "texSampler2D");

        asset = mActivity.getResources().getAssets();

        if(!mModelIsLoaded) {
            mModelIsLoaded = true;
           try {
                mensajes.Load();
                horarios.Load();
                avisos.Load();
                campana.Load();
                documentos.Load();
                mModelIsLoaded = true;
            } catch (IOException e) {
                Log.e(LOGTAG, "Unable to load 3D model");
            }
            // Hide the Loading Dialog
            mActivity.gestorDiag.sendEmptyMessage(GestorDiag.HIDE_LOADING_DIALOG);
        }

      //  mActivity.getSettings();
        mActivity.showImage();
        mActivity.encenderflash();
        mActivity.AbrirCamaraFrontal();
        mActivity.conexionExist();
    }

    public void updateConfiguration()
    {
        mSampleAppRenderer.onConfigurationChanged(mIsActive);
    }
    // The render function called from SampleAppRendering by using RenderingPrimitives views.
    // The state is owned by SampleAppRenderer which is controlling it's lifecycle. State should not be cached outside this method.
    public void renderFrame(State state, float[] projectionMatrix) {

        // Renders video background replacing Renderer.DrawVideoBackground()
        mSampleAppRenderer.renderVideoBackground();

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        // handle face culling, we need to detect if we are using reflection to determine the direction of the culling
        GLES20.glEnable(GLES20.GL_CULL_FACE);
        GLES20.glCullFace(GLES20.GL_BACK);
        int ntarget= state.getNumTrackableResults();

        // Did we find any trackables this frame?
        for (int tIdx = 0; tIdx < ntarget; tIdx++)
        {
            TrackableResult result = state.getTrackableResult(tIdx);
            Trackable trackable = result.getTrackable();

            //****************************AJUSTAR TAMAÑO****************************
            Matrix34F pose = result.getPose();
            float[] base = pose.getData();
            float scale = base[11] / 1;
            if(scale>0.0f && scale<0.55f)
                modelescale = 1.0f;
            if(scale>0.55f && scale<0.8f)
                modelescale = 1.5f;
            if(scale>0.8f && scale<1.3f)
                modelescale = 2.0f;
            if(scale>1.3f )
                modelescale = 3.0f;
            if(scale>1.7f )
                modelescale = 4.0f;

            Matrix44F modelViewMatrix_Vuforia = Tool.convertPose2GLMatrix(result.getPose());
            float[] modelViewMatrixSobre = modelViewMatrix_Vuforia.getData();
            float[] modelViewMatrixTutoria= modelViewMatrix_Vuforia.getData();
            float[] modelViewMatrixAviso = modelViewMatrix_Vuforia.getData();
            float[] modelViewMatrixCampana= modelViewMatrix_Vuforia.getData();
            float[] modelViewMatrixDocumento= modelViewMatrix_Vuforia.getData();

          int orientacion;
            orientacion=mActivity.getOrientation();

            float[] modelViewProjection = new float[16];
            // Se aplican las transformaciones necesarias al modelo
            Matrix.scaleM(modelViewMatrixSobre, 0, modelescale, modelescale, 1.0f);
            Matrix.scaleM(modelViewMatrixTutoria, 0, modelescale, modelescale,1.0f);
            Matrix.scaleM(modelViewMatrixAviso, 0, modelescale, modelescale,1.0f);
            Matrix.scaleM(modelViewMatrixCampana, 0, modelescale, modelescale, 1.0f);
            Matrix.scaleM(modelViewMatrixDocumento, 0, modelescale, modelescale, 1.0f);

            Matrix.multiplyMM(modelViewProjection, 0, projectionMatrix, 0, modelViewMatrixSobre, 0);
            Matrix.multiplyMM(modelViewProjection, 0, projectionMatrix, 0, modelViewMatrixCampana, 0);
            Matrix.multiplyMM(modelViewProjection, 0, projectionMatrix, 0, modelViewMatrixTutoria, 0);
            Matrix.multiplyMM(modelViewProjection, 0, projectionMatrix, 0, modelViewMatrixAviso, 0);
            Matrix.multiplyMM(modelViewProjection, 0, projectionMatrix, 0, modelViewMatrixDocumento, 0);

            // Se activa el shader
            GLES20.glUseProgram(shaderProgramID);

            //**************************RECONOCIMIENTO TARGETS ****************************
            //DESPACHOS
            if (trackable.getName().equalsIgnoreCase("grahambell")) {
                numTarget = 0;
                typetarget = 0;
            }
            else if (trackable.getName().equalsIgnoreCase("benjaminfranklin")){
                numTarget=1;
                typetarget = 0;
            }
            else if (trackable.getName().equalsIgnoreCase("WilliamCullen")){
                numTarget=2;
                typetarget = 0;
            }
            else if (trackable.getName().equalsIgnoreCase("thomasedison")){
                numTarget=3;
                typetarget = 0;
            }
            else if (trackable.getName().equalsIgnoreCase("samuelMorse")){
                    numTarget=4;
                    typetarget = 0;
            }
            else if (trackable.getName().equalsIgnoreCase("nikolatesla")){
                    numTarget=5;
                    typetarget = 0;
                    }
            else if (trackable.getName().equalsIgnoreCase("MichaelFaraday")){
                    numTarget=6;
                    typetarget = 0;
                    }
            else if (trackable.getName().equalsIgnoreCase("MaxwellJamesClerk")){
                     numTarget=7;
                    typetarget = 0;
                    }
            else if (trackable.getName().equalsIgnoreCase("mariecurie")){
                numTarget=8;
                    typetarget = 0;
                    }
            else if (trackable.getName().equalsIgnoreCase("KarlBenz")){
                numTarget=9;
                    typetarget = 0;
                    }
            else if (trackable.getName().equalsIgnoreCase("johnharrison")){
                numTarget=10;
                    typetarget = 0;
                    }
            else if (trackable.getName().equalsIgnoreCase("jethrotull")){
                numTarget=11;
                    typetarget = 0;
                    }
            else if (trackable.getName().equalsIgnoreCase("isaacnewton")){
                numTarget=12;
                typetarget = 0;
                    }
            else if (trackable.getName().equalsIgnoreCase("HumphryDavy")){
                numTarget=13;
                typetarget = 0;
                    }
            else if (trackable.getName().equalsIgnoreCase("gutenberg")){
                numTarget=14;
                typetarget = 0;
                    }
            else if (trackable.getName().equalsIgnoreCase("galileogalilei")){
                numTarget=15;
                typetarget = 0;
                    }
            else if (trackable.getName().equalsIgnoreCase("blaisepascal")){
                numTarget=16;
                typetarget = 0;
                    }
            else if (trackable.getName().equalsIgnoreCase("benjaminfranklin")){
                numTarget=17;
                typetarget = 0;
                    }
            else if (trackable.getName().equalsIgnoreCase("arquimides")){
                numTarget=18;
                typetarget = 0;
                    }
            else if (trackable.getName().equalsIgnoreCase("AlessandroVolta")){
                numTarget=19;
                typetarget = 0;
                    }
            else if (trackable.getName().equalsIgnoreCase("AbrahamDarby")){
                numTarget=20;
                typetarget = 0;
                    }
            //CLASES
            else if (trackable.getName().equalsIgnoreCase("museomadrid")){
                numTarget=0;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museoparis")){
                numTarget=1;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museozurich")){
                numTarget=2;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museoviena")){
                numTarget=3;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museovalencia")){
                numTarget=4;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museotrento")){
                numTarget=5;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museosidney")){
                numTarget=6;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museoshangai")){
                numTarget=7;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museosanfrancisco")){
                numTarget=8;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museoontario")){
                numTarget=9;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museoparis")){
                numTarget=10;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museonuevomexico")){
                numTarget=11;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museonuevazelanda")){
                numTarget=12;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museonuevayork")){
                numTarget=13;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museolucerna")){
                numTarget=14;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museolondres")){
                numTarget=15;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museohouston")){
                numTarget=16;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museohongkong")){
                numTarget=17;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museohelsinki")){
                numTarget=18;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museoguangdong")){
                numTarget=19;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museochicago")){
                numTarget=20;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museoamsterdam")){
                numTarget=21;
                typetarget = 1;
            }
            else if (trackable.getName().equalsIgnoreCase("museoacoruna")){
                numTarget=22;
                typetarget = 1;
            }

            //LABORATORIOS
            else if (trackable.getName().equalsIgnoreCase("inventotelevision")){
                numTarget=0;
                typetarget = 2;
            }
            else if (trackable.getName().equalsIgnoreCase("inventotelegrafo")){
                numTarget=1;
                typetarget = 2;
            }
            else if (trackable.getName().equalsIgnoreCase("inventotelefono")){
                numTarget=2;
                typetarget = 2;
            }
            else if (trackable.getName().equalsIgnoreCase("inventorayosx")){
                numTarget=3;
                typetarget = 2;
            }
            else if (trackable.getName().equalsIgnoreCase("inventopenicilina")){
                numTarget=4;
                typetarget = 2;
            }
            else if (trackable.getName().equalsIgnoreCase("inventoordenador")){
                numTarget=5;
                typetarget = 2;
            }
            else if (trackable.getName().equalsIgnoreCase("inventomicroscopio")){
                numTarget=6;
                typetarget = 2;
            }
            else if (trackable.getName().equalsIgnoreCase("inventomaquinadecoser")){
                numTarget=7;
                typetarget = 2;
            }
            else if (trackable.getName().equalsIgnoreCase("inventoelectricidad")){
                numTarget=8;
                typetarget = 2;
            }
            else if (trackable.getName().equalsIgnoreCase("inventocamara")){
                numTarget=9;
                typetarget = 2;
            }
            else if (trackable.getName().equalsIgnoreCase("inventobrujula")){
                numTarget=10;
                typetarget = 2;
            }

            //*********************************DIBUJOS***************************
            mActivity.AvisoExist(typetarget, numTarget);
            if(typetarget==0)
            {
                mActivity.RecibirAvisos(numTarget);
                avisosActivados=mActivity.getRecibirAvisos();

                if(avisosActivados){
                    mensajes.Draw(modelViewMatrixSobre, projectionMatrix, mTextures, orientacion);
                    horarios.Draw(modelViewMatrixTutoria, projectionMatrix, mTextures);

                    touch=2;
                    if(mActivity.getAvisoExist()) {
                        avisos.Draw(modelViewMatrixAviso, projectionMatrix, mTextures);
                        touch=3;
                    }
                    campana.Draw(modelViewMatrixCampana, projectionMatrix, mTextures, orientacion);
                }
                else
                {
                    horarios.Draw(modelViewMatrixTutoria, projectionMatrix, mTextures);
                    touch=0;
                    if(mActivity.getAvisoExist()) {
                        avisos.Draw(modelViewMatrixAviso, projectionMatrix, mTextures);
                        touch=1;
                    }
                }
            }
            else if(typetarget==1)
            {
                horarios.Draw(modelViewMatrixTutoria, projectionMatrix, mTextures);
                touch=4;
                if(mActivity.getAvisoExist()) {
                    avisos.Draw(modelViewMatrixAviso, projectionMatrix, mTextures);
                    touch=5;
                }
            }
            else if(typetarget==2)
            {
                horarios.Draw(modelViewMatrixTutoria, projectionMatrix, mTextures);
                touch=8;
                if(mActivity.getAvisoExist()) {
                    avisos.Draw(modelViewMatrixAviso, projectionMatrix, mTextures);
                    touch=9;
                }
                documentos.Draw(modelViewMatrixDocumento,projectionMatrix, mTextures);
            }

            ((AppController) mActivity.getApplication()).setTouch(touch);
            mActivity.writeCardTarget(typetarget,numTarget);
            mActivity.showCard();

            if(typetarget==2)
                mActivity.showButtonL();
            if(typetarget==1)
                mActivity.showButtonC();
             if(typetarget==0)
                 mActivity.showButtonP();

            Utiles.checkGLError("Render Frame");
            GLES20.glDisable(GLES20.GL_DEPTH_TEST);;
        }
    }



    public void setTextures(Vector<Textura> texturas)
    {
        mTextures = texturas;
    }
}


