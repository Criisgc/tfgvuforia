package com.example.usuario.vuforia.utiles;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by cgc on 26/03/2018.
 */

public class Objeto {
    public enum DATA_TYPE
    {
        TYPE_VERTEX, TYPE_TEXTURE, TYPE_NORMALS, TYPE_INDEX, TYPE_NULL
    }

    // TEXTURAS                                                                                   */
    public static final int COLOR_AMARILLO = 0;
    public static final int COLOR_AZULCLARO = 1;
    public static final int COLOR_AZULOSCURO= 2;
    public static final int COLOR_BLANCO = 3;
    public static final int COLOR_GRIS = 4;
    public static final int COLOR_MARRON = 5;
    public static final int COLOR_MORADO = 6;
    public static final int COLOR_NEGRO = 7;
    public static final int COLOR_ROJO = 8;
    public static final int COLOR_ROJOCLARO = 9;
    public static final int COLOR_VERDE=10;

    private ByteBuffer verts;
    private ByteBuffer textCoords;
    private ByteBuffer norms;
    private ByteBuffer index;
    private int nVertices = 0;
    private int nVnormal = 0;
    private int nVtext = 0;
    private int nIndices = 0;
    public Objeto (int vertices, int indices)
    {
        nVertices = vertices;
        verts = ByteBuffer.allocateDirect(nVertices * 3 * 4);
        verts.order(ByteOrder.nativeOrder());
        nVtext = vertices * 2;
        textCoords = ByteBuffer.allocateDirect(nVtext * 2 * 4);
        textCoords.order(ByteOrder.nativeOrder());
        nVnormal = vertices * 3;
        norms = ByteBuffer.allocateDirect(nVnormal * 3 * 4);
        norms.order(ByteOrder.nativeOrder());
        nIndices = indices;
        index = ByteBuffer.allocateDirect(nIndices * 4);
        index.order(ByteOrder.nativeOrder());
    }
    public void setData(DATA_TYPE fila, float ndata)
    {
        switch (fila)
        {
            case TYPE_VERTEX:
                verts.putFloat(ndata);
                break;
            case TYPE_TEXTURE:
                textCoords.putFloat(ndata);
                break;
            case TYPE_NORMALS:
                norms.putFloat(ndata);
                break;
            case TYPE_INDEX:
                index.putInt((int)ndata);
                break;
            default:
                break;
        }
    }
    public Buffer getBuffer(DATA_TYPE bufferType)
    {
        Buffer result = null;
        switch (bufferType)
        {
            case TYPE_VERTEX:
                result = verts;
                break;
            case TYPE_TEXTURE:
                result = textCoords;
                break;
            case TYPE_NORMALS:
                result = norms;
                break;
            case TYPE_INDEX:
                result = index;
                break;
            default:
                break;
        }
        return result;
    }

    public int getNumObjectVertex()
    {
        return nVertices;
    }
    public int getNumObjectIndex()
    {
        return nIndices;
    }

    public Buffer getVertices()
    {
        return getBuffer(DATA_TYPE.TYPE_VERTEX);
    }
    public Buffer getTexCoords()
    {
        return getBuffer(DATA_TYPE.TYPE_TEXTURE);
    }
    public Buffer getNormals()
    {
        return getBuffer(DATA_TYPE.TYPE_NORMALS);
    }
    public Buffer getIndex()
    {
        return getBuffer(DATA_TYPE.TYPE_INDEX);
    }

    public void rewind()
    {
        verts.rewind();
        textCoords.rewind();
        norms.rewind();
        index.rewind();
    }
}
