package com.example.usuario;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.usuario.tfgvuforia.R;
import com.example.usuario.tfgvuforia.app.Activities.ActividadAreaProfesores;
import com.example.usuario.tfgvuforia.app.Activities.ActividadMensajesRecibidos;
import com.example.usuario.tfgvuforia.app.Activities.ActividadResponderMensaje;
import com.example.usuario.tfgvuforia.app.Activities.ActividadCamaraPrincipal;
import com.example.usuario.tfgvuforia.app.Activities.ActividadMensajeDespacho;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Locale;

/**
 * Created by cgc on 10/07/2018.
 */

public class AlertDialogManager {

    @SuppressLint("SetTextI18n")
    public void showAlertDialogDeleteMessage(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/futura.ttf");
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog, null);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();

        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        alertDialog.setCancelable(false);
        alertDialog.show();

        TextView textView= (TextView) alertDialog.findViewById(R.id.tituloDialog);
        textView.setTypeface(font);
        textView.setText(R.string.ADborrarMensaje);
        TextView textView1 = (TextView) alertDialog.findViewById(R.id.btn_yes);
        textView1.setTypeface(font);
        textView1.setText(R.string.ADSi);
        TextView textView2 = (TextView) alertDialog.findViewById(R.id.btn_no);
        textView2.setTypeface(font);
        textView2.setText(R.string.ADNo);

        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActividadAreaProfesores ActividadAreaProfesores = new ActividadAreaProfesores();
                ActividadAreaProfesores.deleteMessage(context);
                alertDialog.dismiss();
            }
        });

        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    public void showAlertDialogDeleteMessageRec(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/futura.ttf");
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog, null);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();

        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        alertDialog.setCancelable(false);
        alertDialog.show();

        TextView textView= (TextView) alertDialog.findViewById(R.id.tituloDialog);
        textView.setTypeface(font);
        textView.setText(R.string.ADborrarMensaje);
        TextView textView1 = (TextView) alertDialog.findViewById(R.id.btn_yes);
        textView1.setTypeface(font);
        textView1.setText(R.string.ADSi);
        TextView textView2 = (TextView) alertDialog.findViewById(R.id.btn_no);
        textView2.setTypeface(font);
        textView2.setText(R.string.ADNo);

        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActividadMensajesRecibidos ActividadMensajesRecibidos = new ActividadMensajesRecibidos();
                ActividadMensajesRecibidos.deleteMessage(context);
                alertDialog.dismiss();
            }
        });

        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    public void showAlertDialogDeleteAlert(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/futura.ttf");
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog, null);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();

        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        alertDialog.setCancelable(false);
        alertDialog.show();

        TextView textView= (TextView) alertDialog.findViewById(R.id.tituloDialog);
        textView.setTypeface(font);
        textView.setText(R.string.ADborrarAlerta);
        TextView textView1 = (TextView) alertDialog.findViewById(R.id.btn_yes);
        textView1.setTypeface(font);
        textView1.setText(R.string.ADSi);
        TextView textView2 = (TextView) alertDialog.findViewById(R.id.btn_no);
        textView2.setTypeface(font);
        textView2.setText(R.string.ADNo);

        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActividadAreaProfesores ActividadAreaProfesores = new ActividadAreaProfesores();
                ActividadAreaProfesores.deleteAlert(context);
                alertDialog.dismiss();
            }
        });

        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    public void showAlertDialogResponseMessage(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/futura.ttf");
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog, null);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();

        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        alertDialog.setCancelable(false);
        alertDialog.show();

        TextView textView= (TextView) alertDialog.findViewById(R.id.tituloDialog);
        textView.setTypeface(font);
        textView.setText(R.string.ADmandarMensajeRespuesta);
        TextView textView1 = (TextView) alertDialog.findViewById(R.id.btn_yes);
        textView1.setTypeface(font);
        textView1.setText(R.string.ADSi);
        TextView textView2 = (TextView) alertDialog.findViewById(R.id.btn_no);
        textView2.setTypeface(font);
        textView2.setText(R.string.ADNo);

        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActividadResponderMensaje ActividadResponderMensaje =new ActividadResponderMensaje();
                ActividadResponderMensaje.putMessage(context);
                alertDialog.dismiss();
            }
        });

        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    public void showAlertDialogMessage(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/futura.ttf");
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog, null);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();

        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        alertDialog.setCancelable(false);
        alertDialog.show();

        TextView textView= (TextView) alertDialog.findViewById(R.id.tituloDialog);
        textView.setTypeface(font);
        textView.setText(R.string.ADmandarMensaje);
        TextView textView1 = (TextView) alertDialog.findViewById(R.id.btn_yes);
        textView1.setTypeface(font);
        textView1.setText(R.string.ADSi);
        TextView textView2 = (TextView) alertDialog.findViewById(R.id.btn_no);
        textView2.setTypeface(font);
        textView2.setText(R.string.ADNo);

        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActividadMensajeDespacho ActividadMensajeDespacho =new ActividadMensajeDespacho();
                ActividadMensajeDespacho.putMessage(context);
                alertDialog.dismiss();
            }
        });

        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    public void showAlertDialogEstancia(final Context context, int orientacion) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/futura.ttf");
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textView;
        TextView textView1;
        TextView textView2;
        View view;
        RelativeLayout relativeLayout;
        final AlertDialog alertDialog;

       if(orientacion==1){
            view = inflater.inflate(R.layout.dialog, null);
            relativeLayout= view.findViewById(R.id.alert_layout);
           builder.setView(view);

           alertDialog = builder.create();
           alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
           alertDialog.setCancelable(false);
           alertDialog.show();


           textView= (TextView) alertDialog.findViewById(R.id.tituloDialog);
           textView.setTypeface(font);
           textView.setText(R.string.ADmandarEstancia);
           textView1 = (TextView) alertDialog.findViewById(R.id.btn_yes);
           textView1.setTypeface(font);
           textView1.setText(R.string.ADSi);
           textView2 = (TextView) alertDialog.findViewById(R.id.btn_no);
           textView2.setTypeface(font);
           textView2.setText(R.string.ADNo);
       } else{
           view = inflater.inflate(R.layout.dialog, null);
           relativeLayout= view.findViewById(R.id.alertTotal);
           builder.setView(view);

           alertDialog = builder.create();
           alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
           alertDialog.setCancelable(false);
           alertDialog.show();


           textView= (TextView) alertDialog.findViewById(R.id.tituloDialog);
           textView.setTypeface(font);
           textView.setText(R.string.ADmandarEstancia);
           textView1 = (TextView) alertDialog.findViewById(R.id.btn_yes);
           textView1.setTypeface(font);
           textView1.setText(R.string.ADSi);
           textView2 = (TextView) alertDialog.findViewById(R.id.btn_no);
           textView2.setTypeface(font);
           textView2.setText(R.string.ADNo);
           relativeLayout.setRotation(90.0f);
       }

        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActividadCamaraPrincipal actividadCamaraPrincipal =new ActividadCamaraPrincipal();
                actividadCamaraPrincipal.putEstancia(context);
                alertDialog.dismiss();
            }
        });

        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    public void showAlertDialogAviso(final Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/futura.ttf");
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog, null);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();

        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
        alertDialog.setCancelable(false);
        alertDialog.show();

        TextView textView= (TextView) alertDialog.findViewById(R.id.tituloDialog);
        textView.setTypeface(font);
        textView.setText(R.string.ADmandarAviso);
        TextView textView1 = (TextView) alertDialog.findViewById(R.id.btn_yes);
        textView1.setTypeface(font);
        textView1.setText(R.string.ADSi);
        TextView textView2 = (TextView) alertDialog.findViewById(R.id.btn_no);
        textView2.setTypeface(font);
        textView2.setText(R.string.ADNo);

        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActividadAreaProfesores ActividadAreaProfesores =new ActividadAreaProfesores();
                ActividadAreaProfesores.putAviso(context);
                alertDialog.dismiss();
            }
        });

        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

}
