package com.example.usuario.tfgvuforia.app.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.usuario.AlertDialogManager;
import com.example.usuario.ConexionBBDD.AppController;
import com.example.usuario.tfgvuforia.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cgc on 03/03/2018.
 */

public class ActividadMensajeDespacho extends Activity {

    private static final String LOGTAG = "sendmessageActivity";

    //LAYOUT
    private TextView tituloMensaje;
    private EditText mensaje;
    private Button btnEnviar;

    private String Profesor;
    private String user;
    private Typeface font;

    //CONFIGURACION
    private String ip;

    //BBDD
    private String profesorEnviar;
    private String mensajeEnviar;
    private String url;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mensajesdespacholayout);

        font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");

        Profesor = ((AppController) this.getApplicationContext()).getDestinatario();
        user=((AppController) this.getApplicationContext()).getUser();

        //CONTENIDO DE CONFIGURACION
        ip=((AppController) this.getApplicationContext()).getIp();
        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=" + Profesor + "&pagina=0";

        tituloMensaje = (TextView) findViewById(R.id.tituloEnviarMensaje);
        tituloMensaje.setTypeface(font);
        mensaje = (EditText) findViewById(R.id.textomensaje);
        mensaje.setTypeface(font);
        btnEnviar = (Button) findViewById(R.id.enviar);
        btnEnviar.setTypeface(font);

        btnEnviar.setOnClickListener( new View.OnClickListener(){
                                          @Override
                                          public void onClick(View view) {
                                              mensajeEnviar = mensaje.getText().toString();
                                              AlertDialogManager alertDialogManager= new AlertDialogManager();
                                              alertDialogManager.showAlertDialogMessage(ActividadMensajeDespacho.this);

                                          }
                                      }
        );
    }

    public boolean putMessage(final Context context) {

        Map<String,String> params = new HashMap<String, String>();
        params.put("User", user);
        params.put("mensaje", mensajeEnviar);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if(!((AppController) context.getApplicationContext()).getMockup()){
                            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View layout= inflater.inflate(R.layout.toastlayout, null);
                            TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
                            txtmsg.setText(R.string.MensajeCorrecto);
                            txtmsg.setTypeface(font);
                            Toast toast1 = new Toast(context);
                            toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                            toast1.setView(layout);
                            toast1.show();
                            toast1.setDuration(Toast.LENGTH_SHORT);
                        }
                        else{
                            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View layout= inflater.inflate(R.layout.toastlayout, null);
                            TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
                            txtmsg.setText(R.string.MensajeINCORRECTO);
                            txtmsg.setTypeface(font);
                            Toast toast1 = new Toast(context);
                            toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                            toast1.setView(layout);
                            toast1.show();
                            toast1.setDuration(Toast.LENGTH_SHORT);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(LOGTAG, "dentro de onErrorResponse" + error.getMessage());
                if(!((AppController) context.getApplicationContext()).getMockup()){
                    LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout= inflater.inflate(R.layout.toastlayout, null);
                    TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
                    txtmsg.setText(R.string.MensajeINCORRECTO);
                    txtmsg.setTypeface(font);
                    Toast toast1 = new Toast(context);
                    toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                    toast1.setView(layout);
                    toast1.show();
                    toast1.setDuration(Toast.LENGTH_SHORT);
                }
                else{
                    LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout= inflater.inflate(R.layout.toastlayout, null);
                    TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
                    txtmsg.setText(R.string.MensajeCorrecto);
                    txtmsg.setTypeface(font);
                    Toast toast1 = new Toast(context);
                    toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                    toast1.setView(layout);
                    toast1.show();
                    toast1.setDuration(Toast.LENGTH_SHORT);
                }
            }
        }) {
            @Override
            public Map<String,String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                return params;
            }
        };
        RequestQueue queue= Volley.newRequestQueue(context);
        queue.add(request);
        return true;
    }

}
