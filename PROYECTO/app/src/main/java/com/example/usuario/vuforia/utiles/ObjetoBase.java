package com.example.usuario.vuforia.utiles;

import java.io.IOException;
import java.nio.Buffer;
import java.util.Vector;

/**
 * Created by Usuario on 26/03/2018.
 */

public abstract class ObjetoBase {
    /* Funcinq ue carga un modelo 3D desde el archivo txt                                         */
    public abstract void Load()throws IOException;
    /* Funcion que devuelve los vertices del objeto                                               */
    public abstract Buffer getVertices(int nObjeto);
    /* Funcion de devuelve el numero de vértices del objeto                                       */
    public abstract int getNumObjectVertex(int nObjeto);
    /* Funcion que devuelve el número de indices del objeto                                       */
    public abstract int getNumObjectIndex(int nObjeto);
    /* Funcion que devuelve los indices del objeto                                                */
    public abstract Buffer getIndices(int nObjeto);
    /* Funcion que devuelve las coordenadas de textura del objeto                                 */
    public abstract Buffer getTexCoords(int nObjeto);
    /* Funion que obtiene el buffer de datos solicitados                                          */
    public abstract Buffer getData(Objeto.DATA_TYPE dataType, int nObjeto);
    /* Funcion que dibuja el objeto en el target                                                  */
    public abstract void Draw(float[] modelViewProjection, Vector<Textura> mTextures);
}
