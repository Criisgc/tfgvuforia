package com.example.usuario.Notificaciones;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by cgc on 12/06/2018.
 */

public class NotificationIDTokenService extends FirebaseInstanceIdService {

    private static final String TAG = "NotIdTokenService";

    @Override
    public void onTokenRefresh(){
        Log.d(TAG, "solicitando token");
        String token= FirebaseInstanceId.getInstance().getToken();
        enviarTokenRegistro(token);
    }

    private void enviarTokenRegistro(String token){
        Log.d(TAG, token);
    }
}
