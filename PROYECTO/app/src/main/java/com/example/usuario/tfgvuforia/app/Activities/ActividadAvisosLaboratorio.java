package com.example.usuario.tfgvuforia.app.Activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.usuario.ConexionBBDD.AppController;
import com.example.usuario.ExpandibleBorrar;
import com.example.usuario.ExpandibleSimple;
import com.example.usuario.tfgvuforia.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cgc on 13/06/2018.
 */

public class ActividadAvisosLaboratorio extends Activity {
    private static final String LOGTAG = "avisosLabActivity";

    //LAYOUUT
    private TextView tituloavisos;
    private TextView textoprofesorAviso;
    private ExpandableListView expandableListView;

    private ExpandibleSimple adapter;
    private ArrayList<String> list;
    private Map<String, ArrayList<String>> mapChild;

    private String Laboratorio;
    private String url;

    //BBDD
    private JSONArray data;
    private String nombre;
    private String aviso;

    //CONFIGURACION
    private String ip;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.avisoslaboratoriolayout);

        final Typeface font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");

        tituloavisos = (TextView) findViewById(R.id.titulomostraravisoslab);
        tituloavisos.setTypeface(font);

        Laboratorio = ((AppController) this.getApplicationContext()).getDestinatario();

        //CONTENIDO DE CONFIGURACION
        ip=((AppController) this.getApplicationContext()).getIp();
        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=" + Laboratorio + "&pagina=0";

        expandableListView = (ExpandableListView) findViewById(R.id.textoAvisoslab);
        list = new ArrayList<>();
        mapChild = new HashMap<>();

        getAvisos();
    }

    public void getAvisos() {

        //MOCKUP
        /*************/
        if(((AppController) this.getApplicationContext()).getMockup()){
            if(Laboratorio.equals("Informática")){
                for (int i = 0; i < 1; i++) {
                    if(i==0) {
                        aviso = "Para el laboratorio de Informática 1: Los alumnos que puedan que acudan con su propio ordenador. Gracias";
                    }

                    ArrayList<String> lista = new ArrayList<>();
                    list.add("Aviso " + (i + 1));
                    lista.add(aviso);

                    mapChild.put(list.get(i), lista);
                }
                adapter = new ExpandibleSimple(getApplicationContext(), list, mapChild, 0);
                expandableListView.setAdapter(adapter);
            }
        }
        /*************/
    else{
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(LOGTAG, "dentro de onResponse, url:" + url);

                            try {
                                data = response.getJSONArray("content");
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject obj = data.getJSONObject(i);
                                    aviso = obj.getString("aviso");

                                    ArrayList<String> lista = new ArrayList<>();
                                    list.add("Aviso " + (i + 1));
                                    lista.add(aviso);

                                    mapChild.put(list.get(i), lista);
                                }
                                adapter = new ExpandibleSimple(getApplicationContext(), list, mapChild, 0);
                                expandableListView.setAdapter(adapter);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(LOGTAG, "dentro de onErrorResponse");
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
            };
            AppController.getInstance().addToRequestQueue(jsonObjReq);

        }
    }


}
