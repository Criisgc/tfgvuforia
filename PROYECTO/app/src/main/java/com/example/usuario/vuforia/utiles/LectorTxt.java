package com.example.usuario.vuforia.utiles;

import android.content.res.AssetManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.Vector;

import android.preference.PreferenceManager;
import android.util.Log;

import com.example.usuario.tfgvuforia.app.Activities.ActividadCamaraPrincipal;

public class LectorTxt {
    private static final String LOGTAG = "lectortxt";
    private Objeto ObjetoUnico;
    private int numVerts = 0;
    private int numVertices= 0;
    private int numIndices = 0;
    int numObjtotal = 0;


    public void loadModel(AssetManager assetManager, String filename, Vector<Objeto> VectorObjetos)
            throws IOException {
       // Log.d(LOGTAG, "dentro de loadmodel");
        InputStream is = null;
        Boolean iseof = false;
        try {
            is = assetManager.open(filename);
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(is));
            while (!iseof)
            {
                String line = reader.readLine();
                String word;
                if(line == null)
                {
                    iseof = true;
                }
                else
                {
                    Objeto.DATA_TYPE dataType = Objeto.DATA_TYPE.TYPE_NULL;
                    StringTokenizer st = new StringTokenizer(line);
                    word = st.nextToken();
                    if (word.equals("o"))
                    {
                       // Log.d(LOGTAG, "o");
                        numObjtotal++;
                    }
                    else if (word.equals("numVertices"))
                    {
                       // Log.d(LOGTAG, "numv");
                        word = st.nextToken();
                        numVerts = Integer.parseInt(word);
                    }
                    else if (word.equals("numIndices"))
                    {
                        //Log.d(LOGTAG, "numi");
                        word = st.nextToken();
                        numIndices = Integer.parseInt(word);
                        ObjetoUnico = new Objeto(numVerts, numIndices);
                        VectorObjetos.add(ObjetoUnico);
                    }
                    else if (word.equals("Vertices"))
                    {
                        //Log.d(LOGTAG, "v");
                        dataType = Objeto.DATA_TYPE.TYPE_VERTEX;
                        for (int i = 0; i < numVerts * 3; i++)
                        {
                            ObjetoUnico.setData(dataType, Float.parseFloat(reader.readLine()));
                        }
                    }
                    else if (word.equals("Texturas"))
                    {
                        //Log.d(LOGTAG, "t");
                        dataType = Objeto.DATA_TYPE.TYPE_TEXTURE;
                        for (int i = 0; i < numVerts * 2; i++) {
                            ObjetoUnico.setData(dataType, Float.parseFloat(reader.readLine()));
                        }
                    }
                    else if (word.equals("Normales"))
                    {
                       // Log.d(LOGTAG, "n");
                        dataType = Objeto.DATA_TYPE.TYPE_NORMALS;
                        for (int i = 0; i < numVerts * 3; i++)
                        {
                            ObjetoUnico.setData(dataType, Float.parseFloat(reader.readLine()));
                        }
                    }
                    else if (word.equals("Indices"))
                    {
                       // Log.d(LOGTAG, "i");
                        dataType = Objeto.DATA_TYPE.TYPE_INDEX;
                        for (int i = 0; i < numIndices; i++)
                        {
                            ObjetoUnico.setData(dataType, Integer.parseInt(reader.readLine()));
                        }
                    }
                }
            }
        }
        finally {
            if (is != null)
                is.close();
        }
    }
}
