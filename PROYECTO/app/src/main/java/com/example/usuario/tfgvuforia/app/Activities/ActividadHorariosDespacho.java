package com.example.usuario.tfgvuforia.app.Activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.usuario.ConexionBBDD.AppController;

import com.example.usuario.tfgvuforia.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by cgc on 04/03/2018.
 */

public class ActividadHorariosDespacho extends Activity {

    private static final String LOGTAG = "tutoriasActivity";

   //LAYOUT
    private TextView tituloTutorias;
    private TextView tutorias;

    private String Profesor;
    private String url;

    //CONFIGURACION
    private String ip;

    //BBDD
    private JSONArray data;
    private String nombre;
    private String departamento;
    private String titulacion;
    private String codAsignatura;
    private String asignatura;
    private String grupo;
    private String tipo;
    private String dia;
    private String despacho;
    private String horaIni;
    private String horaFin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.horariosdespacholayout);

        final Typeface font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");

        tituloTutorias = (TextView) findViewById(R.id.tituloHorarioTutoria);
        tituloTutorias.setTypeface(font);
        tutorias = (TextView) findViewById(R.id.textotutorias);
        tutorias.setTypeface(font);

        Profesor = ((AppController) this.getApplicationContext()).getDestinatario();

        //CONTENIDO DE CONFIGURACION
        ip=((AppController) this.getApplicationContext()).getIp();
        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=" + Profesor + "&pagina=0";

        getTutorias();
    }

    public void getTutorias() {
        //MOCKUP
        /**************/
        if(((AppController) this.getApplicationContext()).getMockup()){
           if(Profesor.equals("Maria Esther Palacios Lorenzo")){
               tutorias.setText(getString(R.string.TextoNOMBRETutoriasJSON) + "Maria Esther Palacios Lorenzo" + "\n" +
                       getString(R.string.TextoDespachoTutoriasJSON) + "C302" + "\n" +
                       getString(R.string.TextoDepartamentoTutoriasJSON) + "Quimica" + "\n" +
                       getString(R.string.TitulacionJSON) + "Electrónica" + "\n" +
                       getString(R.string.codAsignaturaJSON) + "1021" + "\n" +
                       getString(R.string.AsignaturaJSON) + "Mecánica de Fluidos" + "\n" +
                       getString(R.string.TipoJSON) + "Problemas" + "\n" +
                       getString(R.string.DiaJSON) + "Martes" + "\n" +
                       getString(R.string.TextoHORAINITutoriasJSON) + "17:00" + "\n" +
                       getString(R.string.TextoHORAFINTutoriasJSON) + "19:00" + "\n");
           }
           else if(Profesor.equals("Gabriel Asensio Madrid")){
               tutorias.setText(getString(R.string.TextoNOMBRETutoriasJSON) + "Gabriel Asensio Madrid" + "\n" +
                       getString(R.string.TextoDespachoTutoriasJSON) + "C103" + "\n" +
                       getString(R.string.TextoDepartamentoTutoriasJSON) + "Matemáticas aplicadas" + "\n" +
                       getString(R.string.TitulacionJSON) + "Mecánica" + "\n" +
                       getString(R.string.codAsignaturaJSON) + "1071" + "\n" +
                       getString(R.string.AsignaturaJSON) + "Ampliación de matemáticas" + "\n" +
                       getString(R.string.TipoJSON) + "Problemas" + "\n" +
                       getString(R.string.DiaJSON) + "Viernes" + "\n" +
                       getString(R.string.TextoHORAINITutoriasJSON) + "12:00" + "\n" +
                       getString(R.string.TextoHORAFINTutoriasJSON) + "14:00" + "\n");
           }
        }
        /**************/

       JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(LOGTAG, "dentro de onResponse, url:" + url);
                        String textoFinal= " ";
                        try {
                            data = response.getJSONArray("content");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject obj = data.getJSONObject(i);
                                nombre = obj.getString("nombre");
                                departamento = obj.getString("departamento");
                                titulacion = obj.getString("titulacion");
                                codAsignatura = obj.getString("codAsignatura");
                                asignatura = obj.getString("asignatura");
                                grupo = obj.getString("grupo");
                                tipo = obj.getString("tipo");
                                dia = obj.getString("dia");
                                despacho = obj.getString("despacho");
                                horaIni = obj.getString("horaIni");
                                horaFin = obj.getString("horaFin");
                                textoFinal=tutorias.getText().toString();
                                tutorias.setText( textoFinal + "\n" +
                                        getString(R.string.TextoNOMBRETutoriasJSON) + nombre + "\n" +
                                        getString(R.string.TextoDespachoTutoriasJSON) + despacho + "\n" +
                                        getString(R.string.TextoDepartamentoTutoriasJSON) + departamento + "\n" +
                                        getString(R.string.TitulacionJSON) + titulacion + "\n" +
                                        getString(R.string.codAsignaturaJSON) + codAsignatura + "\n" +
                                       getString(R.string.AsignaturaJSON) + asignatura + "\n" +
                                        getString(R.string.GrupoJSON)+ grupo + "\n" +
                                        getString(R.string.TipoJSON) + tipo + "\n" +
                                       getString(R.string.DiaJSON) + dia + "\n" +
                                        getString(R.string.TextoHORAINITutoriasJSON) + horaIni + "\n" +
                                        getString(R.string.TextoHORAFINTutoriasJSON) + horaFin + "\n");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(LOGTAG, "dentro de onErrorResponse");
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq);

        tutorias.setMovementMethod(new ScrollingMovementMethod());
    }
    /*    JsonArrayRequest jsonArrayReq = new JsonArrayRequest(urlFinal,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(LOGTAG, "dentro de onResponse, url:" + urlFinal);
                        try {
                            data = response.getJSONObject(0);
                           //for (int i = 0; i < data.length(); i++) {
                              // String datos = i + ":";
                             JSONObject obj=data;
                             //   JSONObject obj = data.getJSONObject(datos);
                                nombre = obj.getString("nombre");
                                departamento = obj.getString("departamento");
                                titulacion = obj.getString("titulacion");
                                codAsignatura = obj.getString("codAsignatura");
                                asignatura = obj.getString("asignatura");
                                grupo = obj.getString("grupo");
                                tipo = obj.getString("tipo");
                                dia = obj.getString("dia");
                                despacho = obj.getString("despacho");
                                horaIni = obj.getString("horaIni");
                                horaFin = obj.getString("horaFin");
                                tutorias.setText(getString(R.string.TextoNOMBRETutoriasJSON) + nombre + "\n" +
                                        getString(R.string.TextoDespachoTutoriasJSON) + despacho + "\n" +
                                        getString(R.string.TextoDepartamentoTutoriasJSON) + departamento + "\n" +
                                        ". Titulacion:" + titulacion + "\n" +
                                        ". Codigo asignatura:" + codAsignatura + "\n" +
                                        ". Asignatura:" + asignatura + "\n" +
                                        ". Grupo:" + grupo + "\n" +
                                        ". Tipo:" + tipo + "\n" +
                                        ". Dia:" + dia + "\n" +
                                        getString(R.string.TextoHORAINITutoriasJSON) + horaIni + "\n" +
                                        getString(R.string.TextoHORAFINTutoriasJSON) + horaFin + "\n");
                           // }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("onErrorResponse", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        AppController.getInstance().addToRequestQueue(jsonArrayReq);
    }*/

}

