package com.example.usuario.tfgvuforia.app.Activities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.usuario.ConexionBBDD.AppController;
import com.example.usuario.ExpandibleBorrarResponder;
import com.example.usuario.tfgvuforia.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

/**
 * Created by cgc on 12/06/2018.
 */

public class ActividadMensajesRecibidos extends Activity{

    private static final String LOGTAG = "ActividadMensajesRecib";

    //LAYOUT
    private TextView tituloMensajes;
    private ExpandableListView expandableListView;

    private ExpandibleBorrarResponder adapter;
    private ArrayList<String> list;
    private Map<String, ArrayList<String>> mapChild;

    private String user;
    private String url;
    private Typeface font;
    private String alumno;
    //BBDD
    private JSONArray data;
    private String nombre;
    private String mensaje;

    //CONFIGURACION
    private String ip;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mensajesrecibidoslayout);

        font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");

        tituloMensajes = (TextView) findViewById(R.id.titulomostrarmensajesrecibidos);
        tituloMensajes.setTypeface(font);

        user = ((AppController) getApplication()).getUser();

        expandableListView = (ExpandableListView) findViewById(R.id.textoMensajesRecibidos);
        list = new ArrayList<>();
        mapChild = new HashMap<>();

        alumno= getIntent().getStringExtra("Alumno");

        //CONTENIDO DE CONFIGURACION
        ip=((AppController) this.getApplicationContext()).getIp();
        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=" + alumno+ "&pagina=0";

        getMensajes();
    }

    public void getMensajes() {
        //MOCKUP
        /*********/
        if(((AppController) this.getApplicationContext()).getMockup()){
            for (int i = 0; i < 1; i++) {

                nombre="Alberto Brunete";
                mensaje="Este martes 28 estaré de 4 a 7 de la tarde en el despacho. Un saludo";

                ArrayList<String> lista = new ArrayList<>();
                list.add(nombre);
                lista.add(mensaje);

                mapChild.put(list.get(i), lista);
            }
            adapter = new ExpandibleBorrarResponder(getApplicationContext(), list, mapChild, ActividadMensajesRecibidos.this, 1);
            expandableListView.setAdapter(adapter);
        }
        /*********/
        else
        {
          JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                  new Response.Listener<JSONObject>() {
                      @Override
                      public void onResponse(JSONObject response) {
                          Log.d(LOGTAG, "dentro de onResponse, url:" + url);
                          try {
                              data = response.getJSONArray("content");
                              for (int i = 0; i < /*data.length()*/1; i++) {
                                  JSONObject obj = data.getJSONObject(i);
                                  nombre = obj.getString("nombre");
                                  mensaje = obj.getString("mensaje");

                                  ArrayList<String> lista = new ArrayList<>();
                                  list.add(nombre);
                                  lista.add(mensaje);

                                  mapChild.put(list.get(i), lista);
                              }
                              adapter = new ExpandibleBorrarResponder(getApplicationContext(), list, mapChild, ActividadMensajesRecibidos.this, 1);
                              expandableListView.setAdapter(adapter);
                          } catch (JSONException e) {
                              e.printStackTrace();
                          }
                      }
                  }, new Response.ErrorListener() {
              @Override
              public void onErrorResponse(VolleyError error) {
                  Log.e(LOGTAG, "dentro de onErrorResponse");
              }
          }) {
              @Override
              public String getBodyContentType() {
                  return "application/json; charset=utf-8";
              }
          };
            AppController.getInstance().addToRequestQueue(jsonObjReq);
        }

    }


    public void deleteMessage(final Context context){

        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=" + user + "&pagina=0";

        Map<String,String> params = new HashMap<String, String>();
        params.put("User", user);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast toast1 = new Toast(context);
                        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout= inflater.inflate(R.layout.toastlayout, null);
                        TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
                        txtmsg.setText(R.string.MensajeBorrado);
                        txtmsg.setTypeface(font);

                        toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                        toast1.setView(layout);
                        toast1.show();
                        toast1.setDuration(Toast.LENGTH_SHORT);

                        getMensajes();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(LOGTAG, "dentro de onErrorResponse" + error.getMessage());

                //MOCKUP
                /*********/
                if(((AppController) context).getMockup()){
                    Toast toast2 = new Toast(context);
                    LayoutInflater inflater2= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout2= inflater2.inflate(R.layout.toastlayout, null);
                    TextView txtmsg2= (TextView) layout2.findViewById(R.id.textoToast);
                    txtmsg2.setText(R.string.MensajeBorrado);
                    txtmsg2.setTypeface(font);

                    toast2.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                    toast2.setView(layout2);
                    toast2.show();
                    toast2.setDuration(Toast.LENGTH_SHORT);
                }
                /*****/
                else{
                    Toast toast1 = new Toast(context);
                    LayoutInflater inflater=  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View layout= inflater.inflate(R.layout.toastlayout, null);
                    TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
                    txtmsg.setText(R.string.ERRORmensajeBorrado);
                    txtmsg.setTypeface(font);

                    toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
                    toast1.setView(layout);
                    toast1.show();
                    toast1.setDuration(Toast.LENGTH_SHORT);
                }
            }
        }) {
            @Override
            public Map<String,String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                return params;
            }
        };
        RequestQueue queue= Volley.newRequestQueue(context);
        queue.add(request);
    }

}
