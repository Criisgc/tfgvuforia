package com.example.usuario.vuforia.utiles;

import android.opengl.GLES20;

import com.example.usuario.tfgvuforia.app.Activities.Renderizador;

import java.io.IOException;
import java.nio.Buffer;
import java.util.Vector;

import android.opengl.Matrix;
import android.util.Log;

/**
 * Created by cgc on 26/03/2018.
 */

public class Mensajes {
    private static final String LOGTAG = "sobre";

    private Vector<Objeto> VectorObjetos = new Vector<>();

    public void Load() throws IOException
    {
        LectorTxt load = new LectorTxt();
        load.loadModel(Renderizador.asset, "sobre2.txt", VectorObjetos);
       // Log.d(LOGTAG, "dentro de load, tamaño objetos:" + VectorObjetos.size());
        for(int i = 0; i < VectorObjetos.size(); ++i)
        {
            Objeto objeto = VectorObjetos.elementAt(i);
            objeto.rewind();
        }
    }

    public Buffer getVertices(int nObjeto)
    {
        return getData(Objeto.DATA_TYPE.TYPE_VERTEX, nObjeto);
    }

    public int getNumObjectVertex(int nObjeto)
    {
        Objeto objeto = VectorObjetos.elementAt(nObjeto);
        return   objeto.getNumObjectVertex();
    }

    public int getNumObjectIndex(int nObjeto)
    {
        Objeto objeto = VectorObjetos.elementAt(nObjeto);
        return   objeto.getNumObjectIndex();
    }

    public Buffer getIndices(int nObjeto)
    {
        return getData(Objeto.DATA_TYPE.TYPE_INDEX, nObjeto);
    }

    public Buffer getTexCoords(int nObjeto)
    {
        return getData(Objeto.DATA_TYPE.TYPE_TEXTURE, nObjeto);
    }

    public Buffer getData(Objeto.DATA_TYPE dataType, int nObjeto)
    {
        Objeto objeto = VectorObjetos.elementAt(nObjeto);
        Buffer result = objeto.getBuffer(dataType);
        return result;
    }

    public void Draw(float[] modelViewMatrixSobre, float[] projectionMatrix, Vector<Textura> mTextures, int orientacion)
    {
        int textureIndex = 0;
        int endObject = VectorObjetos.size();

        //TRANSFORMACIONES
        float[] modelViewMatrixObj1 = modelViewMatrixSobre;
        float[] modelViewMatrixObj2 = modelViewMatrixSobre;
        float[] modelViewMatrixObj3 = modelViewMatrixSobre;
        float[] modelViewMatrixObj4 = modelViewMatrixSobre;
        float[] modelViewMatrixObj5 = modelViewMatrixSobre;

        float[] projectionMatrixObj1 = projectionMatrix;
        float[] projectionMatrixObj2 = projectionMatrix;
        float[] projectionMatrixObj3 = projectionMatrix;
        float[] projectionMatrixObj4 = projectionMatrix;
        float[] projectionMatrixObj5 = projectionMatrix;

        Vector<float[]> modelViewProjection= new Vector<>(5);

        float[] modelViewProjectionObj1 = new float[16];
        float[] modelViewProjectionObj2 = new float[16];
        float[] modelViewProjectionObj3 = new float[16];
        float[] modelViewProjectionObj4 = new float[16];
        float[] modelViewProjectionObj5 = new float[16];

        //sobre
        float objectscale1=0.0005f;
        Matrix.scaleM(modelViewMatrixObj1, 0, objectscale1, objectscale1, objectscale1*0.1f);
        Matrix.translateM(modelViewMatrixObj1, 0, 200.0f, -200.0f, 0.0f);
        if(orientacion==2)
            Matrix.translateM(modelViewMatrixObj1, 0, 0.0f, 100.0f, 0.0f);
        Matrix.rotateM(modelViewMatrixObj1, 0,-90.0f, 0, 0, 1.0f);
        Matrix.multiplyMM(modelViewProjectionObj1, 0, projectionMatrixObj1, 0, modelViewMatrixObj1, 0);
        modelViewProjection.add(modelViewProjectionObj1);
        //borde rojo 1
        float objectscale2=0.6f;
        Matrix.scaleM(modelViewMatrixObj2, 0, objectscale2, objectscale2, 1);
        Matrix.translateM(modelViewMatrixObj2, 0, -80.0f, -10.0f, 120.0f);
        Matrix.rotateM(modelViewMatrixObj2, 0,-60.0f, 0.0f, 0.0f, 1.0f);
        Matrix.multiplyMM(modelViewProjectionObj2, 0, projectionMatrixObj2, 0, modelViewMatrixObj2, 0);
        modelViewProjection.add(modelViewProjectionObj2);
        //borde rojo 2
        Matrix.rotateM(modelViewMatrixObj3, 0, 60.0f, 0.0f, 0.0f, 1.0f);
        Matrix.translateM(modelViewMatrixObj3, 0, 80.0f, -110.0f, 0.0f);
        Matrix.rotateM(modelViewMatrixObj3, 0, 60.0f, 0.0f, 0.0f, 1.0f);
        Matrix.multiplyMM(modelViewProjectionObj3, 0, projectionMatrixObj3, 0, modelViewMatrixObj3, 0);
        modelViewProjection.add(modelViewProjectionObj3);
        //borde gris 1
        float objectscale4=0.6f;
        Matrix.scaleM(modelViewMatrixObj4, 0, objectscale4, objectscale4, 1);
        Matrix.rotateM(modelViewMatrixObj4, 0, -60.0f, 0.0f, 0.0f, 1.0f);
        Matrix.translateM(modelViewMatrixObj4, 0, 0.0f, 50.0f, 0.0f);
        Matrix.rotateM(modelViewMatrixObj4, 0, 40.0f, 0.0f, 0.0f, 1.0f);
        Matrix.multiplyMM(modelViewProjectionObj4, 0, projectionMatrixObj4, 0, modelViewMatrixObj4, 0);
        modelViewProjection.add(modelViewProjectionObj4);
        //borde gris 2
        Matrix.rotateM(modelViewMatrixObj5, 0, -40.0f, 0.0f, 0.0f, 1.0f);
        Matrix.translateM(modelViewMatrixObj5, 0, 0.0f, 170.0f, 0.0f);
        Matrix.rotateM(modelViewMatrixObj5, 0, -55.0f, 0.0f, 0.0f, 1.0f);
        Matrix.translateM(modelViewMatrixObj5, 0, 0.0f, 120.0f, 0.0f);
        Matrix.multiplyMM(modelViewProjectionObj5, 0, projectionMatrixObj5, 0, modelViewMatrixObj5, 0);
        modelViewProjection.add(modelViewProjectionObj5);

        for (int i = 0; i < endObject; ++i)
        {
            if(i==3 || i==4)
                textureIndex = Objeto.COLOR_GRIS;
            if(i==2 || i==1)
                textureIndex = Objeto.COLOR_ROJO;
            if(i==0)
                textureIndex = Objeto.COLOR_BLANCO;

            GLES20.glDisable(GLES20.GL_CULL_FACE);
            GLES20.glVertexAttribPointer(Renderizador.vertexHandle, 3, GLES20.GL_FLOAT, false, 0, getVertices(i));
            GLES20.glVertexAttribPointer(Renderizador.textureCoordHandle, 2, GLES20.GL_FLOAT, false, 0, getTexCoords(i));

            GLES20.glEnableVertexAttribArray(Renderizador.vertexHandle);
            GLES20.glEnableVertexAttribArray(Renderizador.textureCoordHandle);

            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextures.get(textureIndex).mTextureID[0]);

            GLES20.glUniform1i(Renderizador.texSampler2DHandle, 0);

            GLES20.glUniformMatrix4fv(Renderizador.mvpMatrixHandle, 1, false, modelViewProjection.elementAt(i), 0);

            GLES20.glDrawElements(GLES20.GL_TRIANGLES, getNumObjectIndex(i), GLES20.GL_UNSIGNED_INT, getIndices(i));

            GLES20.glDisableVertexAttribArray(Renderizador.vertexHandle);
            GLES20.glDisableVertexAttribArray(Renderizador.textureCoordHandle);
        }
    }
}
