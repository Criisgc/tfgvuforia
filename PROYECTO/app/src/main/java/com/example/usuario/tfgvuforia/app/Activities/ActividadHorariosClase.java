package com.example.usuario.tfgvuforia.app.Activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.usuario.ConexionBBDD.AppController;
import com.example.usuario.tfgvuforia.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by cgc on 04/03/2018.
 */

public class ActividadHorariosClase extends Activity {

    private static final String LOGTAG = "HorarioClasesActivity";

    //LAYOUT
    private TextView tituloHorariosClase;
    private TextView horariosClase;

    private String Clase;
    private String url;

    //CONFIGURACION
    private String ip;

    //BBDD
    private JSONArray data;
    private String titulacion;
    private String codAsignatura;
    private String asignatura;
    private String grupo;
    private String dia;
    private String horaIni;
    private String horaFin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.horariosclaselayout);

        final Typeface font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");

        tituloHorariosClase = (TextView) findViewById(R.id.tituloHorarioClase);
        tituloHorariosClase.setTypeface(font);
        horariosClase = (TextView) findViewById(R.id.textoHorariosClase);
        horariosClase.setTypeface(font);

        Clase = ((AppController) this.getApplicationContext()).getDestinatario();

        //CONTENIDO DE CONFIGURACION
        ip=((AppController) this.getApplicationContext()).getIp();
        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=" + Clase + "&pagina=0";
        getClase();
    }

    public void getClase() {

        //MOCKUP
        /**************/
        if(((AppController) this.getApplicationContext()).getMockup()){
            if(Clase.equals("B11")){
                horariosClase.setText(
                        getString(R.string.TextoCLASEDe) + Clase + "\n" +
                                getString(R.string.DiaJSON) + "Lunes" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Electronica Analógica" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "9:15" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "11:15" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Teoría Máquinas y Mecanismos" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "11:45" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "13:45" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Oficina Técnica" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "15:15" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "17:15" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Electronica Analógica" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "17:45" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "19:45" + "\n" +
                                getString(R.string.DiaJSON) + "Martes" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Regulación Automática " + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "9:15" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "11:15" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Electrónica Digital" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "11:45" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "13:45" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Mecanica de Fluidos" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "15:15" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "17:15" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Electronica Analógica" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "17:45" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "19:45" + "\n" +
                                getString(R.string.DiaJSON) + "Miercoles" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Mecánica de Fluidos" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "9:15" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "11:15" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Regulación Automática" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "11:45" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "13:45" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Oficina Técnica" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "15:15" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "17:15" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Regulación Automática" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "17:45" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "19:45" + "\n" +
                                getString(R.string.DiaJSON) + "Jueves" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Electronica Analógica" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "9:15" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "11:15" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Teoría Máquinas y Mecanismos" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "11:45" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "13:45" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Electrónica Digital" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "15:15" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "17:15" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Electronica Analógica" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "17:45" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "19:45" + "\n" +
                                getString(R.string.DiaJSON) + "Viernes" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Electronica Digital" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "9:15" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "11:15" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Mecánica de Fluidos" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "11:45" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "13:45" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Oficina Técnica" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "15:15" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "17:15" + "\n" +
                                getString(R.string.AsignaturaJSON) + "Regulación Automática" + "\n" +
                                getString(R.string.TextoHORAINITutoriasJSON) + "17:45" +
                                getString(R.string.TextoHORAFINTutoriasJSON) + "19:45" + "\n");
            }
            else if(Clase.equals("A15")){
                horariosClase.setText(getString(R.string.TextoCLASEDe) + Clase + "\n" +
                        getString(R.string.DiaJSON) + "Lunes" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Química" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "9:15" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "11:15" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Física 1" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "11:45" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "13:45" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Física 1" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "15:15" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "17:15" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Dibujo" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "17:45" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "19:45" + "\n" +
                        getString(R.string.DiaJSON) + "Martes" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Dibujo " + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "9:15" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "11:15" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Cálculo 1" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "11:45" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "13:45" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Química" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "15:15" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "17:15" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Dibujo" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "17:45" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "19:45" + "\n" +
                        getString(R.string.DiaJSON) + "Miercoles" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Cálculo 1" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "9:15" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "11:15" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Química" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "11:45" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "13:45" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Algebra" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "15:15" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "17:15" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Física 1" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "17:45" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "19:45" + "\n" +
                        getString(R.string.DiaJSON) + "Jueves" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Física 1" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "9:15" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "11:15" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Algebra" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "11:45" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "13:45" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Cálculo 1" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "15:15" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "17:15" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Química" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "17:45" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "19:45" + "\n" +
                        getString(R.string.DiaJSON) + "Viernes" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Algebra" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "9:15" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "11:15" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Cálculo 1" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "11:45" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "13:45" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Algebra" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "15:15" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "17:15" + "\n" +
                        getString(R.string.AsignaturaJSON) + "Dibujo" + "\n" +
                        getString(R.string.TextoHORAINITutoriasJSON) + "17:45" +
                        getString(R.string.TextoHORAFINTutoriasJSON) + "19:45" + "\n");
            }
        }
        /**************/

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(LOGTAG, "dentro de onResponse, url:" + url);
                        String textoFinal= " ";
                        try {
                            data = response.getJSONArray("content");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject obj = data.getJSONObject(i);
                                titulacion = obj.getString("titulacion");
                                codAsignatura = obj.getString("codAsignatura");
                                asignatura = obj.getString("asignatura");
                                grupo = obj.getString("grupo");
                                dia = obj.getString("dia");
                                horaIni = obj.getString("horaIni");
                                horaFin = obj.getString("horaFin");
                                textoFinal=horariosClase.getText().toString();
                                horariosClase.setText( textoFinal + "\n" +
                                        getString(R.string.TitulacionJSON) + titulacion + "\n" +
                                        getString(R.string.codAsignaturaJSON) + codAsignatura + "\n" +
                                        getString(R.string.AsignaturaJSON) + asignatura + "\n" +
                                        getString(R.string.GrupoJSON) + grupo + "\n" +
                                        getString(R.string.DiaJSON) + dia + "\n" +
                                        getString(R.string.TextoHORAINITutoriasJSON) + horaIni + "\n" +
                                        getString(R.string.TextoHORAFINTutoriasJSON) + horaFin + "\n");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(LOGTAG, "dentro de onErrorResponse");
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq);

        horariosClase.setMovementMethod(new ScrollingMovementMethod());

    }



}
