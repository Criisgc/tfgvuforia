package com.example.usuario.tfgvuforia.app.Activities;

/**
 * Created by cgc on 08/02/2018.
 */
import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.usuario.Authenticator.AccountAuthenticator;
import com.example.usuario.ConexionBBDD.AppController;
import com.example.usuario.tfgvuforia.R;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class ActividadAutenticacion extends Activity /*implements AccountManagerCallback<Bundle>*/ {

    private static final String LOGTAG = "loginActivity";

    public static final String PARAM_CONFIRMCREDENTIALS = "confirmCredentials";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_USERNAME = "username";
    public static final String PARAM_AUTHTOKEN_TYPE = "authorization_code";

    private Button btnLogin;
    private EditText textoEmail;
    private EditText textoPassword;
    private TextView btnRegistro;

    private boolean resultadoAl = false;
    private boolean resultadoPro = false;

    private Typeface font;
    private String url;
    private int role=2;

    //CONFIGURACION
    private String ip;

    private AccountManager mAccountManager;
    private Account mCurrentAccount;
    private Thread mAuthThread;
    private String token;
    private String mAuthtokenType;

    /**
     * If set we are just checking that the user knows their credentials; this
     * doesn't cause the user's password to be changed on the device.
     */
    private Boolean mConfirmCredentials = false;

    /** for posting authentication attempts back to UI thread */
    private final Handler mHandler = new Handler();
    private TextView mMessage;
    private String mPassword;
    private EditText mPasswordEdit;

    /** Was the original caller asking for an entirely new account? */
    protected boolean mRequestNewAccount = false;

    private String mUsername;
    private EditText mUsernameEdit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.autenticacionlayout);

        font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setTypeface(font);
        textoEmail = (EditText) findViewById(R.id.txtEmail);
        textoEmail.setTypeface(font);
        textoPassword= (EditText) findViewById(R.id.txtPass);
        textoPassword.setTypeface(font);
        btnRegistro= (TextView) findViewById(R.id.link_to_register);
        btnRegistro.setTypeface(font);

        ip = ((AppController) this.getApplicationContext()).getIp();
        url = "http://" + ip + ":8081/auth/login";

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itemintent = new Intent(ActividadAutenticacion.this, ActividadRegistro.class);
                finish();
                ActividadAutenticacion.this.startActivity(itemintent);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
            //    com.example.usuario.tfgvuforia.app.Activities.danielsz.OAuth2Client client = new com.example.usuario.tfgvuforia.app.Activities.danielsz.OAuth2Client("cg@alumnos.upm.es", "1234", "ClientId", "secret", url);
            //    Token token = client.getAccessToken();
            //    token.getResource(client, token, "permitAll()");

                String emailCorrectoAl, emailCorrectoPro;
                emailCorrectoAl = "@alumnos.upm.es";
                emailCorrectoPro = "@upm.es";
                String email = textoEmail.getText().toString();
                String password = textoPassword.getText().toString();
                resultadoAl = email.contains(emailCorrectoAl);
                resultadoPro = email.contains(emailCorrectoPro);
                if(resultadoAl)
                    role=0;
                else if(resultadoPro)
                    role=1;

                ((AppController) getApplication()).setRole(role);
                ((AppController) getApplication()).setUser(email);

    if(resultadoPro==true || resultadoAl==true) {
        //MOCKUP
        /**************/
        if(  ((AppController) getApplication()).getMockup()) {
            if ((email.equals("cgc@alumnos.upm.es") || email.equals("jcs@alumnos.upm.es") ||
                    email.equals("pvgm@upm.es") || email.equals("pcgc@upm.es")) && (password.equals("1234"))) {
                Intent itemintent = new Intent(ActividadAutenticacion.this, ActividadOpciones.class);
                finish();
                ActividadAutenticacion.this.startActivity(itemintent);
            } else {
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toastlayout, (ViewGroup) findViewById(R.id.toastlayout));
                TextView txtmsg = (TextView) layout.findViewById(R.id.textoToast);
                txtmsg.setText(R.string.ERRORemailcont);
                txtmsg.setTypeface(font);
                Toast toast1 = new Toast(getApplicationContext());
                toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast1.setView(layout);
                toast1.show();
                toast1.setDuration(Toast.LENGTH_SHORT);
            }
        }
        /*********************/
        else{
            Account account = new Account(email, "com.example.usuario.tfgvuforia");
            AccountManager am= AccountManager.get(ActividadAutenticacion.this);

            am.addAccountExplicitly(account, password, null);
            am.setAuthToken(account, "authorization_code", token);
            /*Bundle options = new Bundle();
            Account account1=null;

            Account[] account = am.getAccountsByType("authorization_code");
            for(int i=0; i<account.length;i++){
                if(account[i].name.equals(email)){
                    String pass= am.getPassword(account[i]);
                    account1=account[i];

                    if(pass.equals(password)){
                        am.getAuthToken(
                                account1,
                                "authorization_code",
                                options,
                                ActividadAutenticacion.this,
                                new OnTokenAdquired(),
                                new Handler(new OnError()));

                      // am.get(ActividadAutenticacion.this).getAuthToken(account1, "authorization_type",true, new OnTokenAdquired(),null);

                    }
                }
            }*/
        }
    }
    else
    {
        LayoutInflater inflater= getLayoutInflater();
        View layout= inflater.inflate(R.layout.toastlayout, (ViewGroup) findViewById(R.id.toastlayout));
        TextView txtmsg= (TextView) layout.findViewById(R.id.textoToast);
        txtmsg.setText(R.string.ERRORextEmail);
        txtmsg.setTypeface(font);
        Toast toast1 = new Toast(getApplicationContext());
        toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL,0,0);
        toast1.setView(layout);
        toast1.show();
        toast1.setDuration(Toast.LENGTH_SHORT);
    }
            }
        });

    }


  /*  @Override
    public void run(AccountManagerFuture<Bundle> accountManagerFuture) {
        if(accountManagerFuture != null)
        {
            try{
                Bundle bundle= accountManagerFuture.getResult();
                if(bundle!=null){
                    Intent intent=bundle.getParcelable(AccountManager.KEY_INTENT);
                    if(intent!=null){
                        Log.d(LOGTAG, "TOKEN2:"+ intent.getStringExtra(AccountManager.KEY_AUTHTOKEN)); //ActividadAutenticacion.TOKEN
                    }
                    else
                        Log.d("PRUEBA", "NO TOKEN");
                }
            }catch (OperationCanceledException e){
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            } catch (AuthenticatorException e) {
                e.printStackTrace();
            };
        }
    }*/

    private class OnError implements Handler.Callback {
        @Override
        public boolean handleMessage(Message msg){
            Log.e("ERROR", "ERRORRRRR");
            return false;
        }

    }

    private class OnTokenAdquired implements AccountManagerCallback<Bundle>{
        @Override
        public void run(AccountManagerFuture<Bundle> result){
            if(result != null) {
                try {
                    Bundle bundle = result.getResult();
                    token = bundle.getString(AccountManager.KEY_AUTHTOKEN);
                    Log.d("TOKEN", ":" + token);
                    if (token != null) {
                        setConexion();
                    }
                    if(bundle!=null){
                        Intent intent=bundle.getParcelable(AccountManager.KEY_INTENT);
                        if(intent!=null){
                            Log.d(LOGTAG, "TOKEN2:"+ intent.getStringExtra(AccountManager.KEY_AUTHTOKEN)); //ActividadAutenticacion.TOKEN
                       setConexion();
                        }
                        else
                            Log.d("PRUEBA", "NO TOKEN");
                    }

                } catch (OperationCanceledException e) {
                    Log.e("E", "1");
                } catch (AuthenticatorException e) {
                    Log.e("E", "2");
                } catch (IOException e) {
                    Log.e("E", "3");
                }
            }
        }
    }

    public void setConexion(){
        URL url= null;
        Log.d("BU", "BU");
        try {
            Log.d("TRY", "URL");
            url = new URL("http://192.168.1.133:8081/auth/login");
        } catch (MalformedURLException e) {
            Log.e("ERROR", ":" + e);
        }
        URLConnection conn= null;
        try {
            Log.d("TRY", "CONN");
            conn = (HttpURLConnection) url.openConnection();
            conn.addRequestProperty("client_id", "CliendId");
            conn.addRequestProperty("client_secret", "secret");
            conn.setRequestProperty("Authorization", "OAUTH_TOKEN" + token);
        } catch (IOException e) {
            Log.e("ERROR", ":" + e);
        }

    }
}

