package com.example.usuario;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.text.method.ScrollingMovementMethod;

import com.example.usuario.tfgvuforia.R;
import com.example.usuario.tfgvuforia.app.Activities.ActividadResponderMensaje;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by cgc on 10/07/2018.
 */

public class ExpandibleBorrarResponder extends BaseExpandableListAdapter{

    private ArrayList<String> listTitulo;
    private Map<String, ArrayList<String>> mapChild;
    private Context context;
    private Activity activity;
    private int tipo;
    private String nombre;

    public ExpandibleBorrarResponder(Context context, ArrayList<String> listTitulo, Map<String, ArrayList<String>> mapChild, Activity activity, int tipo){
        this.context=context;
        this.listTitulo=listTitulo;
        this.mapChild=mapChild;
        this.activity= activity;
        this.tipo=tipo;
    }

    @Override
    public int getGroupCount() {

        return listTitulo.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return mapChild.get(listTitulo.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return listTitulo.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return mapChild.get(listTitulo.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {

        final Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/futura.ttf");

        nombre = (String) getGroup(i);
        view= LayoutInflater.from(context).inflate(R.layout.expandablegroup, null);
        TextView textViewGroup = (TextView) view.findViewById(R.id.ExpanGroup);
        textViewGroup.setText(nombre);
        textViewGroup.setTypeface(font);
        return view;
    }

    @Override
    public View getChildView(final int i, final int i1, boolean b, View view, ViewGroup viewGroup) {
        final Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/futura.ttf");

        String mensaje = (String) getChild(i,i1);
        view= LayoutInflater.from(context).inflate(R.layout.expandablechildmessageresp, null);
        TextView textViewChild = view.findViewById(R.id.ExpanChildMessageText);
        textViewChild.setText(mensaje);
        textViewChild.setTypeface(font);
        final Button buttonChild = view.findViewById(R.id.ButtonChildMessage);
        ImageButton buttonDelete = view.findViewById(R.id.ButtonDeleteMessage);
        if(tipo==1) {
            buttonChild.setVisibility(View.INVISIBLE);
            buttonDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialogManager alertDialogManager= new AlertDialogManager();
                    alertDialogManager.showAlertDialogDeleteMessageRec(context);
                }
            });
        }
        else{
            buttonChild.setTypeface(font);
            buttonChild.setTextSize(9.0f);
            buttonChild.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(activity, ActividadResponderMensaje.class);
                    intent.putExtra("Alumno", nombre);
                    activity.startActivity(intent);

                }
            });

            buttonDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialogManager alertDialogManager= new AlertDialogManager();
                    alertDialogManager.showAlertDialogDeleteMessage(context);
                }
            });

        }

        textViewChild.setMovementMethod(new ScrollingMovementMethod());
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
