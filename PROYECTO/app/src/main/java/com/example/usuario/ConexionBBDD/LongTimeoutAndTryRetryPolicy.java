package com.example.usuario.ConexionBBDD;

import com.android.volley.DefaultRetryPolicy;

/**
 * Created by cgc on 12/06/2018.
 */

public class LongTimeoutAndTryRetryPolicy extends DefaultRetryPolicy {

    public static final int TIMEOUT_MS = 2000;
    public static final int RETRIES_PHONE_ISP = 3;

    public LongTimeoutAndTryRetryPolicy(int retries) {
        super(TIMEOUT_MS, retries, DEFAULT_BACKOFF_MULT);
    }
}