package com.example.usuario.tfgvuforia.app.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.usuario.ConexionBBDD.AppController;
import com.example.usuario.tfgvuforia.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by cgc on 26/01/2018.
 */

public class ActividadOpciones extends Activity {

    private static final String LOGTAG = "optionsActivity";

    //LAYOUT
    private ImageButton BotonCamara;
    private ImageButton Configuracion;
    private ImageButton Informacion;
    private TextView Camara;
    private TextView Informaciont;
    private TextView Configuraciont;
    private TextView UsuarioLogged;
    private ImageButton BotonMensajes;
    private ImageButton BotonAreaProfesores;
    private TextView AreaProfesores;
    private TextView Mensajes;

    private String user;

    private String url;
    private String ip;
    //SETTINGS
    private SharedPreferences sharedPref;
    private Configuration config = new Configuration();
    private boolean prefNotificaciones;

    private int role;
    public JSONArray data;
    private String roledesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        AppController.getInstance().onConfigurationChanged(config);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.opcioneslayout);

        final Typeface font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");

        user= ((AppController) this.getApplicationContext()).getUser();
        ip = ((AppController) this.getApplicationContext()).getIp();
        role = ((AppController) this.getApplicationContext()).getRole();
        url= "http://" + ip + ":1111/profesoresPorNombre?nombre=";

        UsuarioLogged = (TextView) findViewById(R.id.userLogged);
        UsuarioLogged.setTypeface(font);
        UsuarioLogged.setText(user);

        getResources().getConfiguration();

        Camara = (TextView) findViewById(R.id.textAbrirCamara);
        Camara.setTypeface(font);
        Informaciont = (TextView) findViewById(R.id.textinformacion);
        Informaciont.setTypeface(font);
        Configuraciont = (TextView) findViewById(R.id.textConfiguracion);
        Configuraciont.setTypeface(font);
        BotonMensajes = (ImageButton) findViewById(R.id.ButtonMessageOptions);
        BotonAreaProfesores = (ImageButton) findViewById(R.id.AreapButton);
        AreaProfesores = (TextView) findViewById(R.id.textAreap);
        AreaProfesores.setTypeface(font);
        Mensajes = (TextView) findViewById(R.id.textMensajesRespondidos);
        Mensajes.setTypeface(font);

        if(role==0)
        {
            getMensajes();
        }
        else if(role==1)
        {
         AreaProfesores.setVisibility(View.VISIBLE);
         BotonAreaProfesores.setVisibility(View.VISIBLE);
         BotonAreaProfesores.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View view) {
                                                   Intent i = new Intent(ActividadOpciones.this, ActividadAreaProfesores.class);
                                                   i.putExtra("Usuario", user);
                                                   startActivity(i);
                                               }
                                           }

            );

        }

        BotonCamara = (ImageButton) findViewById(R.id.abrirCamara);
        BotonCamara.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View view) {
                                               Intent i = new Intent(ActividadOpciones.this, ActividadCamaraPrincipal.class);
                                               startActivity(i);
                                           }
                                       }

        );

        Configuracion = (ImageButton) findViewById(R.id.configuracion);
        Configuracion.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View view) {
                                                 Intent i = new Intent(ActividadOpciones.this, ActividadConfiguracion.class);
                                                 i.putExtra("Role", role);
                                                 startActivity(i);
                                             }
                                         }

        );

        Informacion = (ImageButton) findViewById(R.id.Informacion);
        Informacion.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View view) {
                                               Intent i = new Intent(ActividadOpciones.this, ActividadInformacion.class);
                                               startActivity(i);
                                           }
                                       }
        );

        //CONTENIDO DE CONFIGURACION
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        prefNotificaciones = sharedPref.getBoolean("Notificaciones", false);
        if(prefNotificaciones)
            lanzarNotificaciones();


    }


    public void getMensajes() {
        //MOCKUP
        /*************/
        if(((AppController) getApplication()).getMockup()) {
            if (user.contains("cgc@alumnos.upm.es")) {
                BotonMensajes.setVisibility(View.VISIBLE);
                Mensajes.setVisibility(View.VISIBLE);
                BotonMensajes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(ActividadOpciones.this, ActividadMensajesRecibidos.class);
                        startActivity(i);
                    }
                });
            } else {
                BotonMensajes.setVisibility(View.INVISIBLE);
                Mensajes.setVisibility(View.INVISIBLE);
            }
        }
        /************/

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            data = response.getJSONArray("content");
                            if(data.length()>0) {
                                BotonMensajes.setVisibility(View.VISIBLE);
                                Mensajes.setVisibility(View.VISIBLE);
                                BotonMensajes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent i = new Intent(ActividadOpciones.this, ActividadMensajesRecibidos.class);
                                        i.putExtra("Usuario", user);
                                        startActivity(i);
                                    }
                                });
                            }
                            else {
                                BotonMensajes.setVisibility(View.INVISIBLE);
                                Mensajes.setVisibility(View.INVISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(LOGTAG, "dentro de onErrorResponse getMensajes");
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjReq);

    }

    public void lanzarNotificaciones(){
        String token= FirebaseInstanceId.getInstance().getToken();
        enviarTokenRegistro(token);
    }

    public void enviarTokenRegistro(String token){
        Log.d(LOGTAG, "TOKEN FCB: " + token);
    }
}