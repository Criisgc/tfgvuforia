package com.example.usuario.ConexionBBDD;

import android.app.Application;
import android.graphics.Bitmap;
//import android.support.multidex.MultiDex;
import android.support.v4.util.LruCache;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by cgc on 12/06/2018.
 */

public class AppController extends Application {


    /**
     * Log or request TAG
     */
    public static final String TAG = "Aument i";

    /**
     * Image cache size.
     */
    public static final int IMAGE_CACHE_SIZE = 10;

    /**
     * Global request queue for Volley
     */
    private RequestQueue mRequestQueue;

    /**
     *
     */
    private ImageLoader mImageLoader;

    /**
     * A singleton instance of the application class for easy access in other places
     */
    private static AppController sInstance;

/*
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    */

    @Override
    public void onCreate() {
        super.onCreate();

        // initialize the singleton
        sInstance = this;

      /*  SharedPreferences sharedPref= PreferenceManager.getDefaultSharedPreferences(this);
        String idioma=sharedPref.getString("Idioma", "DEFAULT");
        switch(idioma){
            case "es": {
                Locale esp = new Locale("es");
                Locale.setDefault(esp);
                Configuration config= new Configuration();
                config.locale = esp;
                getResources().updateConfiguration(config, null);
            }
            case "en":{
                Locale eng = new Locale("en");
                Locale.setDefault(eng);
                Configuration config= new Configuration();
                config.locale = eng;
                getResources().updateConfiguration(config, null);
            }
        }*/
    }

    /**
     * @return ApplicationController singleton instance
     */
    public static synchronized AppController getInstance() {
        return sInstance;
    }

    /**
     * @return The Volley Request queue, the queue will be created if it is null
     */
    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    /**
     * @return The Volley image loader, if null will be created.
     */
    public ImageLoader getImageLoader(){
        if (mImageLoader == null){
            mImageLoader = new ImageLoader(getRequestQueue(), new ImageLoader.ImageCache() {
                private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(IMAGE_CACHE_SIZE);

                public void putBitmap(String url, Bitmap bitmap) {
                    mCache.put(url, bitmap);
                }

                public Bitmap getBitmap(String url) {
                    return mCache.get(url);
                }
            });
        }
        return mImageLoader;
    }

    /**
     * Adds the specified request to the global queue, if tag is specified
     * then it is used else Default TAG is used.
     *
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        getRequestQueue().add(req);
    }

    /**
     * Adds the specified request to the global queue using the Default TAG.
     *
     * @param req
     */
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);

        getRequestQueue().add(req);
    }

    /**
     * Cancels all pending requests by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled.
     *
     * @param tag
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    private String user;
    public void setUser( String u){
        user=u;
    }
    public String getUser() {
        return user;
    }

    private int role;
    public void setRole(int r){
       role=r; //1:ALUMNO 0:PROFESOR
    }
    public int getRole(){return role;}

    private String destinatario;
    public void setDestinatario( String d){
        destinatario=d;
    }
    public String getDestinatario() {
        return destinatario;
    }

    private int touch;
    public void setTouch(int t)
    {
        touch=t;
    }
    public int getTouch(){
        return touch;
    }

    private String ip;
    public void setIp(String ip){
        this.ip=ip;
    }
    public String getIp() {
        return ip;
    }

    private boolean mockup=false;
    public boolean getMockup(){return mockup;}
}
