/*===============================================================================
Copyright (c) 2016-2017 PTC Inc. All Rights Reserved.


Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package com.example.usuario.tfgvuforia.app.Activities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.usuario.AlertDialogManager;
import com.example.usuario.ConexionBBDD.AppController;
import com.example.usuario.vuforia.utiles.Textura;
import com.vuforia.CameraDevice;
import com.vuforia.DataSet;
import com.vuforia.ObjectTracker;
import com.vuforia.State;
import com.vuforia.STORAGE_TYPE;
import com.vuforia.Trackable;
import com.vuforia.Tracker;
import com.vuforia.TrackerManager;
import com.vuforia.Vuforia;
import com.example.usuario.vuforia.SampleApplicationControl;
import com.example.usuario.vuforia.SampleApplicationException;
import com.example.usuario.vuforia.SampleApplicationSession;
import com.example.usuario.vuforia.utiles.GestorDiag;
import com.example.usuario.vuforia.utiles.ApplicationGLView;
import com.example.usuario.tfgvuforia.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ActividadCamaraPrincipal extends Activity implements SampleApplicationControl
{
    private static final String LOGTAG = "ActividadCamara";
    
    SampleApplicationSession vuforiaAppSession;
    
    private DataSet mCurrentDataset;
    private int mCurrentDatasetSelectionIndex = 0;
    private ArrayList<String> mDatasetStrings = new ArrayList<String>();
    
    // Our OpenGL view:
    private ApplicationGLView mGlView;
    // Our renderer:
    private Renderizador mRenderer;
    // The textures we will use for rendering:
    private Vector<Textura> mTexturas;

    private RelativeLayout mUILayout;

    GestorDiag gestorDiag = new GestorDiag(this);
    boolean mIsDroidDevice = false;

    /*Detectar touch en pantalla*/
    private GestureDetector mGestureDetector;

    private String url;
    //GLOBAL NUMTARGET
    private int GlobalnumTarget;
    private String user;

    private boolean mSwitchDatasetAsap = false;
    private boolean mContAutofocus = true;
    private boolean result = true;
    private boolean mExtendedTracking = false;

    private RelativeLayout cardLayout;
    private int orientacion;

   //CARD
    private View _viewCard;
    private TextView _textType;
    private TextView _textValue;
    private ImageView _instanceImageView;
    private Button _botonSobreCampana;
    private View LogoApp;

    private int typeTarget;
    private Typeface font;

    //CONEXION BBDD
    private ImageView ConexionOFF;
    private ImageView ConexionON;

    //CONFIGURACION
    private boolean prefFlash;
    private SharedPreferences sharedPref;
    private boolean CamFrontal;
    private String ip;

    //BBDD
   private JSONArray data;
   private String aviso;

    // Called when the activity first starts or the user navigates back to anactivity.
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        /*Se obtienen los datos configurables de la app*/
        orientacion=getResources().getConfiguration().orientation; //1:ver 2:hor
        user= ((AppController) this.getApplicationContext()).getUser();
        sharedPref= PreferenceManager.getDefaultSharedPreferences(ActividadCamaraPrincipal.this);
        prefFlash= sharedPref.getBoolean("Flash",false);
        CamFrontal=sharedPref.getBoolean("CamaraFrontal", false);
        ip=sharedPref.getString("ip", "DEFAULT");
        url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=";

        vuforiaAppSession = new SampleApplicationSession(this);
        
        startLoadingAnimation();
        mDatasetStrings.add("TFG.xml");

       vuforiaAppSession.initAR(this, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        
        mGestureDetector = new GestureDetector(this, new GestureListener());
        
        // Load any sample specific textures:
        mTexturas = new Vector<Textura>();
        loadTextures();
        mIsDroidDevice = android.os.Build.MODEL.toLowerCase().startsWith("droid");

       font = Typeface.createFromAsset(getAssets(), "fonts/futura.ttf");

        startLayout();

        /*cardLayout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    hideCard();
                    return true;
                }
                return false;
            }
        });*/
    }

    // We want to load specific textures from the APK, which we will later use for rendering.
    private void loadTextures()
    {
        mTexturas.add(Textura.loadTextureFromApk("Texturas/amarillo.png", getAssets()));
        mTexturas.add(Textura.loadTextureFromApk("Texturas/azulClaro.png", getAssets()));
        mTexturas.add(Textura.loadTextureFromApk("Texturas/azulLetra.png", getAssets()));
        mTexturas.add(Textura.loadTextureFromApk("Texturas/blanco.png", getAssets()));
        mTexturas.add(Textura.loadTextureFromApk("Texturas/grisRaya.png", getAssets()));
        mTexturas.add(Textura.loadTextureFromApk("Texturas/marron.png", getAssets()));
        mTexturas.add(Textura.loadTextureFromApk("Texturas/morado.png", getAssets()));
        mTexturas.add(Textura.loadTextureFromApk("Texturas/negro.png", getAssets()));
        mTexturas.add(Textura.loadTextureFromApk("Texturas/rojo.png", getAssets()));
        mTexturas.add(Textura.loadTextureFromApk("Texturas/rojoSobre.png", getAssets()));
        mTexturas.add(Textura.loadTextureFromApk("Texturas/verde.png", getAssets()));
        mTexturas.add(Textura.loadTextureFromApk("Texturas/campanatext2.png", getAssets()));
        mTexturas.add(Textura.loadTextureFromApk("Texturas/foliotext.png", getAssets()));
        mTexturas.add(Textura.loadTextureFromApk("Texturas/corchotext.jpg", getAssets()));
    }

    // Process Single Tap event to trigger autofocus
    private class GestureListener extends GestureDetector.SimpleOnGestureListener
    {
        // Used to set autofocus one second after a manual focus is triggered
      /*  private final Handler autofocusHandler = new Handler();
        @Override
        public boolean onDown(MotionEvent e)
        {
            return true;
        }
        @Override
        public boolean onSingleTapUp(MotionEvent e)
        {
            boolean result = CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO);
            if (!result)
                Log.e("SingleTapUp", "Unable to trigger focus");
            // Generates a Handler to trigger continuous auto-focus after 1 second
            autofocusHandler.postDelayed(new Runnable()
            {
                public void run()
                {
                    if (mContAutofocus)
                    {
                        final boolean autofocusResult = CameraDevice.getInstance().setFocusMode(
                                CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO);
                        if (!autofocusResult)
                            Log.e("SingleTapUp", "Unable to re-enable continuous auto-focus");
                    }
                }
            }, 1000L);
            return true;
        }*/
    }

    // Called when the activity will start interacting with the user.
    @Override
    protected void onResume()
    {
        super.onResume();
        // This is needed for some Droid devices to force portrait
        if (mIsDroidDevice)
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        try
        {
            vuforiaAppSession.resumeAR();
        } catch (SampleApplicationException e)
        {
            Log.e(LOGTAG, e.getString());
        }
        /* Continua el GLView                                                                               */
        if (mGlView != null)
        {
            mGlView.setVisibility(View.VISIBLE);
            mGlView.onResume();
        }
    }
    // Callback for configuration changes the activity handles itself
    @Override
    public void onConfigurationChanged(Configuration config)
    {
        super.onConfigurationChanged(config);
        vuforiaAppSession.onConfigurationChanged();
    }
    // Called when the system is about to start resuming a previous activity.
    @Override
    protected void onPause()
    {
        super.onPause();
        if (mGlView != null)
        {
            mGlView.setVisibility(View.INVISIBLE);
            mGlView.onPause();
        }
        try
        {
            vuforiaAppSession.pauseAR();
        } catch (SampleApplicationException e)
        {
            Log.e(LOGTAG, e.getString());
        }
    }
    // The final call you receive before your activity is destroyed.
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        
        try
        {
            vuforiaAppSession.stopAR();
        } catch (SampleApplicationException e)
        {
            Log.e(LOGTAG, e.getString());
        }
        // Unload texture:
        mTexturas.clear();
        mTexturas = null;
        System.gc();
    }

    // Initializes AR application components.
    private void initApplicationAR()
    {
        // Create OpenGL ES view:
        int depthSize = 16;
        int stencilSize = 0;
        boolean translucent = Vuforia.requiresAlpha();
        
        mGlView = new ApplicationGLView(this);
        mGlView.init(translucent, depthSize, stencilSize);
        mRenderer = new Renderizador(this, vuforiaAppSession);
        mRenderer.setTextures(mTexturas);
        mGlView.setRenderer(mRenderer);
    }

    private void startLoadingAnimation()
    {
        mUILayout = (RelativeLayout) View.inflate(this, R.layout.camera_overlay,null);
        mUILayout.setVisibility(View.VISIBLE);
        mUILayout.setBackgroundColor(Color.BLACK);
        // Gets a reference to the loading dialog
        gestorDiag.mLoadingDialogContainer = mUILayout.findViewById(R.id.loading_indicator);
        // Shows the loading indicator at start
        gestorDiag.sendEmptyMessage(GestorDiag.SHOW_LOADING_DIALOG);
        // Adds the inflated layout to the view
        addContentView(mUILayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    }

    private void startLayout(){
        LayoutParams layoutParamsControl = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        LayoutInflater inflater = getLayoutInflater();

        if(orientacion==1)
        {
            LogoApp=inflater.inflate(R.layout.sobrelayout,null);
            LogoApp.setVisibility(View.INVISIBLE);
            _viewCard = inflater.inflate(R.layout.card, null);
            _viewCard.setVisibility(View.INVISIBLE);
            cardLayout = (RelativeLayout) _viewCard.findViewById(R.id.card_layout);

            //CONTENIDO DE TAPARLOGO
            addContentView(LogoApp, layoutParamsControl);

            //CONTENIDO DE CARD
            addContentView(_viewCard, layoutParamsControl);
            _textType = (TextView) _viewCard.findViewById(R.id.text_type);
            _textType.setTypeface(font);
            _textValue = (TextView) _viewCard.findViewById(R.id.text_value);
            _textValue.setTypeface(font);
            _instanceImageView = (ImageView) _viewCard.findViewById(R.id.taparlogo);
            _botonSobreCampana= (Button) _viewCard.findViewById(R.id.btnSobreCampana);
            _botonSobreCampana.setTypeface(font);

            //CONTENIDO DE CONEXIONBBDD
            ConexionOFF = (ImageView) LogoApp.findViewById(R.id.conOff);
            ConexionON = (ImageView) LogoApp.findViewById(R.id.conOn);
            ConexionON.setVisibility(View.INVISIBLE);
            ConexionOFF.setVisibility(View.INVISIBLE);

        }
        else if(orientacion==2)
        {
            LogoApp=inflater.inflate(R.layout.sobrelayoutland,null);
            LogoApp.setVisibility(View.INVISIBLE);
            _viewCard = inflater.inflate(R.layout.card, null);
            cardLayout = (RelativeLayout) _viewCard.findViewById(R.id.card_layout);
            cardLayout.setRotation(90.0f);
            cardLayout.setScaleX(1.45f);
            cardLayout.setTranslationX(-240.0f);
            cardLayout.setTranslationY(-540.0f);
            _viewCard.setVisibility(View.INVISIBLE);

            //CONTENIDO DE TAPARLOGO
            addContentView(LogoApp, layoutParamsControl);

            //CONTENIDO DE CARD
            addContentView(_viewCard, layoutParamsControl);
            _textType = (TextView) _viewCard.findViewById(R.id.text_type);
            _textType.setTypeface(font);
            _textValue = (TextView) _viewCard.findViewById(R.id.text_value);
            _textValue.setTypeface(font);
            _instanceImageView = (ImageView) _viewCard.findViewById(R.id.taparlogoland);
            _botonSobreCampana= (Button) _viewCard.findViewById(R.id.btnSobreCampana);
            _botonSobreCampana.setTypeface(font);

            //CONTENIDO DE CONEXIONBBDD
            ConexionOFF = (ImageView) LogoApp.findViewById(R.id.conOffland);
            ConexionON = (ImageView) LogoApp.findViewById(R.id.conOnland);
            ConexionON.setVisibility(View.INVISIBLE);
            ConexionOFF.setVisibility(View.INVISIBLE);
        }
    }

    // Methods to load and destroy tracking data.
    @Override
    public boolean doLoadTrackersData()
    {

        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager.getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (mCurrentDataset == null)
            mCurrentDataset = objectTracker.createDataSet();

        if (mCurrentDataset == null)
            return false;

        if (!mCurrentDataset.load(mDatasetStrings.get(mCurrentDatasetSelectionIndex),
                STORAGE_TYPE.STORAGE_APPRESOURCE))
            return false;

        if (!objectTracker.activateDataSet(mCurrentDataset))
            return false;

        int numTrackables = mCurrentDataset.getNumTrackables();
        for (int count = 0; count < numTrackables; count++)
        {
            Trackable trackable = mCurrentDataset.getTrackable(count);
            if(isExtendedTrackingActive())
            {
                trackable.startExtendedTracking();
            }

            String name = "Current Dataset : " + trackable.getName();
            trackable.setUserData(name);
        }


        return true;
    }

    @Override
    public boolean doUnloadTrackersData()
    {
        // Indicate if the trackers were unloaded correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (mCurrentDataset != null && mCurrentDataset.isActive())
        {
            if (objectTracker.getActiveDataSet(0).equals(mCurrentDataset)
                    && !objectTracker.deactivateDataSet(mCurrentDataset))
            {
                result = false;
            } else if (!objectTracker.destroyDataSet(mCurrentDataset))
            {
                result = false;
            }

            mCurrentDataset = null;
        }

        return result;
    }


    void encenderflash(){
       /* runOnUiThread (new Runnable(){
            @Override
            public void run(){*/
                if(prefFlash) {
                    result = CameraDevice.getInstance().setFlashTorchMode(true);
                }
         /*   }
        });*/
    }
    void AbrirCamaraFrontal(){
      /*  runOnUiThread (new Runnable(){
            @Override
            public void run(){*/
                if(CamFrontal) {
                    result = CameraDevice.getInstance().setFlashTorchMode(false);
                    vuforiaAppSession.stopCamera();
                    try {
                        vuforiaAppSession.startAR( CameraDevice.CAMERA_DIRECTION.CAMERA_DIRECTION_FRONT);
                    } catch (SampleApplicationException e) {
                        e.printStackTrace();
                    }
                }
           /* }
        });*/
    }

    //CONEXIONBBDD
    void showConexion(final boolean conexion){
        runOnUiThread (new Runnable(){
            @Override
            public void run(){
                if(conexion) {
                    ConexionON.setVisibility(View.VISIBLE);
                    ConexionOFF.setVisibility(View.INVISIBLE);
                }
                else{
                    ConexionON.setVisibility(View.INVISIBLE);
                    ConexionOFF.setVisibility(View.VISIBLE);

                }
           }
        });
    }

    void AvisoExist(final int typeTarget, final int numTarget)
    {
       String tipo="";
        if (typeTarget==0)
           tipo = setTeacher(numTarget);
        else if(typeTarget==1)
            tipo=setClass(numTarget);
        else if(typeTarget==2)
            tipo=setLaboratorio(numTarget);


        new RedAccionTask().execute("Aviso",tipo);

        //MOCKUP
        /**************/
      /*  if( ((AppController) getApplication()).getMockup()){
            if (tipo.equals("Gabriel Asensio Madrid")){
                setAvisoExist(false);
            }
            else if(tipo.equals("Maria Esther Palacios Lorenzo")){
                setAvisoExist(true);
            }
           else if (tipo.equals("B11")){
                setAvisoExist(true);
            }
            else if(tipo.equals("A15")){
                setAvisoExist(false);
            }
           else if (tipo.equals("Informática")){
                setAvisoExist(true);
            }
            else if(tipo.equals("Electrónica")){
                setAvisoExist(false);
            }
            else
                setAvisoExist(false);
        }*/
        /**************/
       /* else{
            url = "http://192.168.1.107:1111/inicio/all";
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(LOGTAG, "dentro de onResponse, url:" + url);
                            try {
                                data = response.getJSONArray("content");
                                if(data.length()>0){
                                    setAvisoExist(true);
                                }
                                else{
                                    setAvisoExist(false);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
            };
            AppController.getInstance().addToRequestQueue(jsonObjReq);
        }*/
    }

    boolean avisoExist;
    void setAvisoExist(boolean avEx){
        avisoExist=avEx;
    }
    boolean getAvisoExist(){
        return avisoExist;
    }

    void conexionExist()
    {
        new RedAccionTask().execute("Conexion");

      /*  final boolean mockup=((AppController) this.getApplicationContext()).getMockup();
        runOnUiThread (new Runnable(){
            @Override
            public void run(){
                url = "http://192.168.1.107:1111/inicio/all";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(LOGTAG, "dentro de onResponse, url:" + url);
                    if(!mockup){
                        showConexion(true);
                    }
                    else{
                        showConexion(false);
                    }
                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.e(LOGTAG, "dentro de onErrorResponse");
            if(!mockup){
                showConexion(false);
            }
            else{
                showConexion(true);
            }
        }
    }) {
        @Override
        public String getBodyContentType() {
            return "application/json; charset=utf-8";
        }
    };
        AppController.getInstance().addToRequestQueue(jsonObjReq);
            }
        });*/
    }

    void RecibirAvisos( int numEtiqueta ){
        final String Profesor;
        Profesor=setTeacher(numEtiqueta);

        new RedAccionTask().execute("RecibirAviso", Profesor);

        //MOCKUP
        /**************/
      /*  if( ((AppController) getApplication()).getMockup()){
            if (Profesor.equals("Gabriel Asensio Madrid")){
                setRecibirAvisos(true);
            }
            else if(Profesor.equals("Maria Esther Palacios Lorenzo")){
                setRecibirAvisos(false);
            }

        }*/
        /**************/
       /* else{
            runOnUiThread (new Runnable(){
                @Override
                public void run() {
                    url = "http://192.168.1.107:1111/inicio/all" + Profesor;
                    JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.d(LOGTAG, "dentro de onResponse, url:" + url);
                                    try {
                                        data = response.getJSONArray("content");
                                        for (int i = 0; i < data.length(); i++) {
                                            JSONObject obj = data.getJSONObject(i);
                                            aviso = obj.getString("RecibirAviso");
                                            if(aviso.equals("Sí"))
                                                setRecibirAvisos(true);
                                            else
                                                setRecibirAvisos(false);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e(LOGTAG, "dentro de onErrorResponse");
                        }
                    }) {
                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8";
                        }
                    };
                    AppController.getInstance().addToRequestQueue(jsonObjReq);
                }
            });
        }*/
    }
    boolean recibirAvisos;
    void setRecibirAvisos(boolean recAv){
        recibirAvisos=recAv;
    }
    boolean getRecibirAvisos(){
        return recibirAvisos;
    }

    //FUNCION TAPAR LOGO
    void showImage(){
         runOnUiThread (new Runnable(){
            @Override
            public void run(){

                LogoApp.setVisibility(View.VISIBLE);
                LogoApp.bringToFront();
           }
        });
    }
    //FUNCIONES CARD
    void showCard() {
        final Context context = this;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // if scard is already visible with same VuMark, do nothing
                if (_viewCard.getVisibility() == View.VISIBLE) {
                    return;
                }
               Animation bottomUp = AnimationUtils.loadAnimation(context, R.anim.bottom_up);
               // _viewCard.startAnimation(bottomUp);
                _viewCard.bringToFront();
                _viewCard.setVisibility(View.VISIBLE);
                _textType.setText(" ");
                _textValue.setText(" ");
                _botonSobreCampana.setVisibility(View.INVISIBLE);
            }
        });
    }

    void hideCard() {
        final Context context = this;
       runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // if card not visible, do nothing
                if (_viewCard.getVisibility() != View.VISIBLE) {
                    return;
                }
                Animation bottomDown = AnimationUtils.loadAnimation(context, R.anim.bottom_down);
                _viewCard.startAnimation(bottomDown);
                _viewCard.setVisibility(View.INVISIBLE);

          }
        });
    }

    void showButtonP(){
      typeTarget=0;
        final Context context = this;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                _viewCard.bringToFront();
            }
        });
    }
    void showButtonC(){
        typeTarget=1;
        final Context context = this;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                _viewCard.bringToFront();
            }
        });

    }
    void showButtonL(){
        typeTarget=2;
        final Context context = this;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                _viewCard.bringToFront();
            }
        });
    }

    void writeCardP(final int numVirtualButton){
        runOnUiThread(new Runnable() { @Override public void run() {
            _botonSobreCampana.setVisibility(View.VISIBLE);
                              switch (numVirtualButton) {
                                  case 1: {
                                      _textValue.setText(R.string.TextoSiAVISARProfesor);
                                      _botonSobreCampana.setOnClickListener(new View.OnClickListener() {
                                          public void onClick(View view) {
                                              if(orientacion==2){
                                                  putEstancia(getApplicationContext());
                                                   }
                                              else if(orientacion==1){
                                                  AlertDialogManager alertDialogManager= new AlertDialogManager();
                                                  alertDialogManager.showAlertDialogEstancia(ActividadCamaraPrincipal.this, orientacion);
                                              }
                                                   }
                                      });
                                      break;
                                  }
                                  case 2: {
                                      _textValue.setText(R.string.TextoSiMENSAJEProfesor);
                                      _botonSobreCampana.setOnClickListener(new View.OnClickListener() {
                                          public void onClick(View view) {
                                              Intent inte = new Intent(ActividadCamaraPrincipal.this, ActividadMensajeDespacho.class);
                                              String Profesor;
                                              Profesor=setTeacher(GlobalnumTarget);
                                              ((AppController) getApplication()).setDestinatario(Profesor);
                                              ActividadCamaraPrincipal.this.startActivity(inte);
                                          }
                                      });
                                      break;
                                  }
                                  case 3: {
                                      _textValue.setText(R.string.TextoSiTUTORIASProfesor);
                                      _botonSobreCampana.setOnClickListener(new View.OnClickListener() {
                                          public void onClick(View view) {
                                              String Profesor;
                                              Profesor=setTeacher(GlobalnumTarget);
                                              ((AppController) getApplication()).setDestinatario(Profesor);
                                              Intent inte = new Intent(ActividadCamaraPrincipal.this, ActividadHorariosDespacho.class);
                                              ActividadCamaraPrincipal.this.startActivity(inte);
                                          }
                                      });
                                      break;
                                  }
                                  case 4: {
                                      _textValue.setText(R.string.TextoSiVERAVISOProfesor);
                                      _botonSobreCampana.setOnClickListener(new View.OnClickListener() {
                                          public void onClick(View view) {
                                              Intent inte = new Intent(ActividadCamaraPrincipal.this, ActividadAvisosDespacho.class);
                                              String Profesor;
                                              Profesor=setTeacher(GlobalnumTarget);
                                              ((AppController) getApplication()).setDestinatario(Profesor);
                                              ActividadCamaraPrincipal.this.startActivity(inte);
                                          }
                                      });
                                      break;
                                  }
                              }
                         }
                      });
    }
    void writeCardC(final int numVirtualButton){
        runOnUiThread(new Runnable() { @Override public void run() {
            _botonSobreCampana.setVisibility(View.VISIBLE);
            switch (numVirtualButton) {
                case 1: {
                    _textValue.setText(R.string.TextoSiHORARIOClase);
                    _botonSobreCampana.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            Intent inte = new Intent(ActividadCamaraPrincipal.this, ActividadHorariosClase.class);
                            String clase=setClass(GlobalnumTarget);
                            ((AppController) getApplication()).setDestinatario(clase);
                            ActividadCamaraPrincipal.this.startActivity(inte);
                        }
                    });
                    break;
                }
                case 2: {
                    _textValue.setText(R.string.TextoSiAVISOClase);
                    _botonSobreCampana.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            Intent inte = new Intent(ActividadCamaraPrincipal.this, ActividadAvisosClase.class);
                            String clase=setClass(GlobalnumTarget);
                            ((AppController) getApplication()).setDestinatario(clase);
                            ActividadCamaraPrincipal.this.startActivity(inte);
                        }
                    });
                    break;
                }
            }
        }
        });
    }
    void writeCardL(final int numVirtualButton){
        runOnUiThread(new Runnable() { @Override public void run() {
            _botonSobreCampana.setVisibility(View.VISIBLE);
            switch (numVirtualButton) {
                case 1: {
                    _textValue.setText(R.string.horariosLAB);
                    _botonSobreCampana.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            Intent inte = new Intent(ActividadCamaraPrincipal.this, ActividadHorariosLaboratorio.class);
                            String laboratorio=setLaboratorio(GlobalnumTarget);
                            ((AppController) getApplication()).setDestinatario(laboratorio);
                            ActividadCamaraPrincipal.this.startActivity(inte);
                        }
                    });
                    break;
                }
                case 2: {
                    _textValue.setText(R.string.TextoSiAVISOClase);
                    _botonSobreCampana.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            Intent inte = new Intent(ActividadCamaraPrincipal.this, ActividadAvisosLaboratorio.class);
                            String laboratorio=setLaboratorio(GlobalnumTarget);
                            ((AppController) getApplication()).setDestinatario(laboratorio);
                            ActividadCamaraPrincipal.this.startActivity(inte);
                        }
                    });
                    break;
                }
                case 3: {
                    _textValue.setText(R.string.DocumentosLAB);
                    _botonSobreCampana.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            Intent inte = new Intent(ActividadCamaraPrincipal.this, ActividadDocumentosLaboratorio.class);
                            String laboratorio=setLaboratorio(GlobalnumTarget);
                            ((AppController) getApplication()).setDestinatario(laboratorio);
                            ActividadCamaraPrincipal.this.startActivity(inte);
                        }
                    });
                    break;
                }
            }
        }
        });
    }

    void writeCardTarget(final int typeTarget, final int numTarget){
        runOnUiThread(new Runnable() { @Override public void run() {
            String Profesor;
            GlobalnumTarget=numTarget;
            Profesor=setTeacher(numTarget);

            if(typeTarget==0){
                _textType.setText(getString(R.string.TextoDESPACHODEProfesores) + Profesor);
            }
            else if(typeTarget==1){
                _textType.setText(getString(R.string.TextoCLASEDe) + setClass(numTarget));
            }
            else if(typeTarget==2){
                _textType.setText(getString(R.string.TextoLABOde) + setLaboratorio(numTarget));
            }

        }
        });
    }

    String setTeacher(final int numTarget){
        String Profesor=new String();
        switch(numTarget){
            case 0:{
                Profesor=new String("Maria Esther Palacios Lorenzo");
                break;
            }
            case 1:{
                Profesor=new String("Juan Manuel Rodriguez Nuevo");
                break;
            }
            case 2:{
                Profesor=new String("Jose Ignacio Vicario Lopez");
                break;
            }
            case 3:{
                Profesor=new String("Isabel Alvaro Hernando");
                break;
            }
            case 4: {
                Profesor = new String("Gabriel Asensio Madrid");
                break;
            }
            case 5:{
                Profesor=new String("Irene Martín Rubio");
                break;
            }
            case 6:{
                Profesor=new String("Isabel Carrillo Ramiro");
                break;
            }
            case 7:{
                Profesor=new String("Antonio Zanon Ballesteros");
                break;
            }
            case 8:{
                Profesor=new String("Pablo San Segundo Carrillo");
                break;
            }
            case 9:{
                Profesor=new String("Carlos Platero Dueñas");
                break;
            }
            case 10:{
                Profesor=new String("Jesus Perez Sanz");
                break;
            }
            case 11:{
                Profesor=new String("Federico Javier Muñoz Cano");
                break;
            }
            case 12:{
                Profesor=new String("Juan Mario García de Maria");
                break;
            }
            case 13:{
                Profesor=new String("Cecilia Elisabet García Cena");
                break;
            }
            case 14:{
                Profesor=new String("Luis Dávila Gomez");
                break;
            }
            case 15:{
                Profesor=new String("Cristobal Colón Hernandez");
                break;
            }
            case 16:{
                Profesor=new String("Pedro Luis Castedo Cepeda");
                break;
            }
            case 17:{
                Profesor=new String("Raquel Cedazo León");
                break;
            }
            case 18:{
                Profesor=new String("Alberto Brunete Gonzalez");
                break;
            }
            case 19:{
                Profesor=new String("Esther Andres Perez");
                break;
            }
            case 20:{
                Profesor=new String("Basil Mohammed Al-Hadithi Abdul Qadir");
                break;
            }
        }
            return Profesor;
    }
    String setClass(final int numTarget){
        switch(numTarget){
            case 0:
                return "B11";
            case 1:
                return "B12";
            case 2:
                return "B21";
            case 3:
                return "B22";
            case 4:
                return "B31";
            case 5:
                return "B32";
            case 6:
                return "B41";
            case 7:
                return "B41";
            case 8:
                return "A10";
            case 9:
                return "A11";
            case 10:
                return "A12";
            case 11:
                return "A13";
            case 12:
                return "A14";
            case 13:
                return "A15";
            case 14:
                return "A16";
            case 15:
                return "A17";
            case 16:
                return "A18";
            case 17:
                return "A19";
            case 18:
                return "A20";
            case 19:
                return "A21";
            case 20:
                return "A22";
        }
        return null;
    }
    String setLaboratorio(final int numTarget){
        switch(numTarget){
            case 0:
                return getString(R.string.LABCienciaMat);
            case 1:
                return getString(R.string.LABElectronica);
            case 2:
                return getString(R.string.LABFabricacion);
            case 3:
                return getString(R.string.LABFisica);
            case 4:
                return getString(R.string.LABInformatica);
            case 5:
                return getString(R.string.LABMaqElec);
            case 6:
                return getString(R.string.LABQuimica);
            case 7:
                return "Lab1";
            case 8:
                return "Lab2";
            case 9:
                return "Lab3";
            case 10:
                return "Lab4";
            case 11:
                return "Lab5";
            case 12:
                return "Lab6";
            case 13:
                return "Lab7";
            case 14:
                return "Lab8";

        }
        return null;
    }

    public void putEstancia(final Context context) {

         url="http://" + ip + "1111/inicio/profesoresPorNombre?nombre=" + "&pagina=0";

        Map<String, String> params = new HashMap<String, String>();
        params.put("User", user);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(!((AppController) context.getApplicationContext()).getMockup()){
                    if(getOrientation()==1) {
                        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout = inflater.inflate(R.layout.toastlayout, null);
                        TextView txtmsg = (TextView) layout.findViewById(R.id.textoToast);
                        txtmsg.setText(R.string.estanciaCorrecta);
                        txtmsg.setTypeface(font);
                        Toast toast1 = new Toast(context);
                        toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast1.setView(layout);
                        toast1.show();
                        toast1.setDuration(Toast.LENGTH_SHORT);
                    }
                    }
                    else{
                    if(getOrientation()==1) {
                        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout = inflater.inflate(R.layout.toastlayout, null);
                        TextView txtmsg = (TextView) layout.findViewById(R.id.textoToast);
                        txtmsg.setText(R.string.estanciaINCORRECTA);
                        txtmsg.setTypeface(font);
                        Toast toast1 = new Toast(context);
                        toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast1.setView(layout);
                        toast1.show();
                        toast1.setDuration(Toast.LENGTH_SHORT);
                    }
                    }

                    }
                    }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(LOGTAG, "dentro de onErrorResponse" + error.getMessage());
                if(!((AppController) context.getApplicationContext()).getMockup()){
                    if(getOrientation()==1) {
                        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout = inflater.inflate(R.layout.toastlayout, null);
                        TextView txtmsg = (TextView) layout.findViewById(R.id.textoToast);
                        txtmsg.setText(R.string.estanciaINCORRECTA);
                        txtmsg.setTypeface(font);
                        Toast toast1 = new Toast(context);
                        toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast1.setView(layout);
                        toast1.show();
                        toast1.setDuration(Toast.LENGTH_SHORT);
                    }
                    }
                    else{
                    if(getOrientation()==0) {
                        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View layout = inflater.inflate(R.layout.toastlayout, null);
                        TextView txtmsg = (TextView) layout.findViewById(R.id.textoToast);
                        txtmsg.setText(R.string.estanciaCorrecta);
                        txtmsg.setTypeface(font);
                        Toast toast1 = new Toast(context);
                        toast1.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast1.setView(layout);
                        toast1.show();
                        toast1.setDuration(Toast.LENGTH_SHORT);
                    }
                    }
                    }
                    }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                return params;
                }
                };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }

    @Override
    public void onVuforiaResumed()
    {
        if (mGlView != null)
        {
            mGlView.setVisibility(View.VISIBLE);
            mGlView.onResume();
        }
    }

    @Override
    public void onVuforiaStarted()
    {
        mRenderer.updateConfiguration();
        if (mContAutofocus)
        {
            // Set camera focus mode
            if(!CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO))
            {
                // If continuous autofocus mode fails, attempt to set to a different mode
                if(!CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO))
                {
                    CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_NORMAL);
                }
            }
        }
        showProgressIndicator(false);
    }

    public void showProgressIndicator(boolean show)
    {
        if (gestorDiag != null)
        {
            if (show)
            {
                gestorDiag.sendEmptyMessage(GestorDiag.SHOW_LOADING_DIALOG);
            }
            else
            {
                gestorDiag.sendEmptyMessage(GestorDiag.HIDE_LOADING_DIALOG);
            }
        }
    }

    @Override
    public void onInitARDone(SampleApplicationException exception)
    {
        if (exception == null)
        {
            initApplicationAR();
            
            mRenderer.setActive(true);
            
            // Now add the GL surface view. It is important that the OpenGL ES surface view gets added BEFORE the camera
            // is started and video background is configured.
            addContentView(mGlView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            // Sets the UILayout to be drawn in front of the camera
            mUILayout.bringToFront();
            // Sets the layout background to transparent
            mUILayout.setBackgroundColor(Color.TRANSPARENT);
            try
            {
                vuforiaAppSession.startAR(CameraDevice.CAMERA_DIRECTION.CAMERA_DIRECTION_DEFAULT);
            } catch (SampleApplicationException e)
            {
                Log.e(LOGTAG, e.getString());
            }

        } else {
            Log.e(LOGTAG, exception.getString());
            showInitializationErrorMessage(exception.getString());
        }
    }
    // Shows initialization error messages as System dialogs
    public void showInitializationErrorMessage(String message)
    {
        final String errorMessage = message;
    }
    @Override
    public void onVuforiaUpdate(State state)
    {
        if (mSwitchDatasetAsap)
        {
            mSwitchDatasetAsap = false;
            TrackerManager tm = TrackerManager.getInstance();
            ObjectTracker ot = (ObjectTracker) tm.getTracker(ObjectTracker.getClassType());
            if (ot == null || mCurrentDataset == null || ot.getActiveDataSet(0) == null)
            {
                Log.d(LOGTAG, "Failed to swap datasets");
                return;
            }
            doUnloadTrackersData();
            doLoadTrackersData();
        }
    }

    @Override
    public boolean doInitTrackers()
    {
        // Indicate if the trackers were initialized correctly
        boolean result = true;
        TrackerManager tManager = TrackerManager.getInstance();
        Tracker tracker;
        // Trying to initialize the image tracker
        tracker = tManager.initTracker(ObjectTracker.getClassType());
        if (tracker == null)
        {
            Log.e(
                LOGTAG, "Tracker not initialized. Tracker already initialized or the camera is already started");
            result = false;
        } else
        {
            Log.i(LOGTAG, "Tracker successfully initialized");
        }
        return result;
    }
    @Override
    public boolean doStartTrackers()
    {
        // Indicate if the trackers were started correctly
        boolean result = true;
        Tracker objectTracker = TrackerManager.getInstance().getTracker(ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.start();

        return result;
    }
    @Override
    public boolean doStopTrackers()
    {
        // Indicate if the trackers were stopped correctly
        boolean result = true;
        Tracker objectTracker = TrackerManager.getInstance().getTracker(
            ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.stop();
        return result;
    }
    @Override
    public boolean doDeinitTrackers()
    {
        // Indicate if the trackers were deinitialized correctly
        boolean result = true;
        TrackerManager tManager = TrackerManager.getInstance();
        tManager.deinitTracker(ObjectTracker.getClassType());
        return result;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        float x = event.getX();
        float y = event.getY();

        int touch = ((AppController) this.getApplicationContext()).getTouch();

        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN :
                Log.d(LOGTAG, "ACTION_DOWN  x:" + x + "  y:" + y);
                if (CamFrontal == false)
                {
                   if(orientacion==2)
                   {
                       if(x>200.0f && x<400.0f && y>650.0f && y<1000.0f) //MENSAJES
                       {
                           if(typeTarget==0 && (touch==2 || touch==3))
                               writeCardP(2);
                           if(typeTarget==2 && (touch==9 || touch==8))
                               writeCardL(3);
                       }
                       else if(x>200.0f && x<400.0f && y>100.0f && y<450.0f) //CAMPANA
                       {
                           if(typeTarget==0 && (touch==2 || touch==3))
                               writeCardP(1);

                       }
                       else if(x<700.0f && x>500.0f && y>100.0f && y<450.0f) //AVISOS
                       {
                           if(typeTarget==0 && (touch==1 || touch==3))
                               writeCardP(4);
                           if(typeTarget==1 && (touch==5))
                               writeCardC(2);
                           if(typeTarget==2 && (touch==7 || touch==9))
                               writeCardL(2);
                       }
                       else if(x<700.0f && x>500.0f && y>650.0f && y<1000.0f) //TUTORIAS
                       {
                           if(typeTarget==0)
                               writeCardP(3);
                           if(typeTarget==1)
                               writeCardC(1);
                           if(typeTarget==2)
                               writeCardL(1);
                       }
                   }
                   else if(orientacion==1)
                   {
                       if(x<720.0f && x>420.0f && y>0.0f && y<450.0f) //TUTORIAS
                       {
                           if(typeTarget==0)
                               writeCardP(3);
                           if(typeTarget==1)
                               writeCardC(1);
                           if(typeTarget==2)
                               writeCardL(1);
                       }
                       else if(x<720.0f && x>420.0f && y>550.0f && y<950.0f)  //MENSAJES
                       {
                           if(typeTarget==0 && (touch==2 || touch==3))
                               writeCardP(2);
                           if(typeTarget==2 && (touch==9 || touch==8))
                               writeCardL(3);
                       }
                       else if(x>0.0f && x<300.0f && y>550.0f && y<950.0f) //CAMPANA
                       {
                           if(typeTarget==0 && (touch==2 || touch==3))
                               writeCardP(1);

                       }
                       else if(x>0.0f && x<300.0f && y>0.0f && y<450.0f)  //AVISOS
                       {
                           if(typeTarget==0 && (touch==1 || touch==3))
                               writeCardP(4);
                           if(typeTarget==1 && (touch==5))
                               writeCardC(2);
                           if(typeTarget==2 && (touch==7 || touch==9))
                               writeCardL(2);
                       }
                   }
                }
                else {
                    if(orientacion==2)
                    {
                        if (x < 400.0f && x >0.0f && y > 900.0f && y < 1200.0f) //CAMPANA
                        {
                            if (typeTarget == 0 && (touch==2 || touch==3))
                                writeCardP(1);
                        } else if (x > 600.0f && x < 1200.0f && y > 0.0f && y < 400.0f) //CALENDARIO
                        {
                            if (typeTarget == 0)
                                writeCardP(3);
                            if (typeTarget == 1)
                                writeCardC(1);
                            if (typeTarget == 2)
                                writeCardL(1);

                        } else if (x < 1000.0f && x > 600.0f && y > 900.0f && y < 1200.0f) //AVISO
                        {
                            if (typeTarget == 0 && (touch==1 || touch==3))
                                writeCardP(4);
                            if (typeTarget == 1 && (touch==5))
                                writeCardC(2);
                            if (typeTarget == 2 && (touch==7 || touch==9))
                                writeCardL(2);
                        } else if (x > 0.0f && x < 400.0f && y > 0.0f && y < 300.0f) //MENSAJE
                        {
                            if (typeTarget == 0 && (touch==2 || touch==3))
                                writeCardP(2);
                            if (typeTarget == 2 && (touch==9 || touch==8))
                                writeCardL(3);
                        }
                    }
                    else if(orientacion==1)
                    {
                        if (x < 650.0f && x > 450.0f && y > 650.0f && y < 950.0f) //CAMPANA
                        {
                            if (typeTarget == 0 && (touch==2 || touch==3))
                                writeCardP(1);
                        } else if (x < 650.0f && x > 450.0f && y > 0.0f && y < 400.0f) //AVISO
                        {
                            if(typeTarget==0 && (touch==1 || touch==3))
                                writeCardP(4);
                            if(typeTarget==1 && (touch==5))
                                writeCardC(2);
                            if(typeTarget==2 && (touch==7 || touch==9))
                                writeCardL(2);
                        } else if (x > 0.0f && x < 200.0f && y > 0.0f && y < 400.0f) //CALENDARIO
                        {
                            if (typeTarget == 0)
                                writeCardP(3);
                            if (typeTarget == 1)
                                writeCardC(1);
                            if (typeTarget == 2)
                                writeCardL(1);
                        } else if (x > 0.0f && x < 200.0f && y > 650.0f && y < 950.0f) //MENSAJE
                        {
                            if (typeTarget == 0 && (touch==2 || touch==3))
                                writeCardP(2);
                            if (typeTarget == 2 && (touch==9 || touch==8))
                                writeCardL(3);
                        }
                    }
                }
        }
        // Process the Gestures
        return mGestureDetector.onTouchEvent(event);
    }

    boolean isExtendedTrackingActive()
    {
        return mExtendedTracking;
    }

    public int getOrientation()
    {
        return orientacion;
    }



    /* Se realizan las operaciones de red de forma asincrona para no bloquear el UI
       params[0]: tipo de peticion que se realiza
       params[1]: Lugar al que se pide                                                                */
    public class RedAccionTask extends AsyncTask<String , String, String>
    {

        @Override
        protected String doInBackground(String... params){

            if(params[0].equals("Aviso")){
                String tipo=params[1];
                //MOCKUP
                /**************/
                if( ((AppController) getApplication()).getMockup()){
                    if (tipo.equals("Gabriel Asensio Madrid")){
                        setAvisoExist(false);
                    }
                    else if(tipo.equals("Maria Esther Palacios Lorenzo")){
                        setAvisoExist(true);
                    }
                    else if (tipo.equals("B11")){
                        setAvisoExist(true);
                    }
                    else if(tipo.equals("A15")){
                        setAvisoExist(false);
                    }
                    else if (tipo.equals("Informática")){
                        setAvisoExist(true);
                    }
                    else if(tipo.equals("Electrónica")){
                        setAvisoExist(false);
                    }
                    else
                        setAvisoExist(false);
                }
                /**************/
                else{
                    url = "http://192.168.1.107:1111/inicio/all" + tipo;
                    JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.d(LOGTAG, "dentro de onResponse, url:" + url);
                                    try {
                                        data = response.getJSONArray("content");
                                        if(data.length()>0){
                                            setAvisoExist(true);
                                        }
                                        else{
                                            setAvisoExist(false);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8";
                        }
                    };
                    AppController.getInstance().addToRequestQueue(jsonObjReq);
                }
            }
            else if(params[0].equals("Conexion")){
                final boolean mockup=((AppController) getApplication()).getMockup();
                        url = "http://192.168.1.107:1111/inicio/all";
                        JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.d(LOGTAG, "dentro de onResponse, url:" + url);
                                        if(!mockup){
                                            showConexion(true);
                                        }
                                        else{
                                            showConexion(false);
                                        }
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e(LOGTAG, "dentro de onErrorResponse");
                                if(!mockup){
                                    showConexion(false);
                                }
                                else{
                                    showConexion(true);
                                }
                            }
                        }) {
                            @Override
                            public String getBodyContentType() {
                                return "application/json; charset=utf-8";
                            }
                        };
                        AppController.getInstance().addToRequestQueue(jsonObjReq);
            }
            else if(params[0].equals("RecibirAviso")){
                //MOCKUP
                /**************/
                if( ((AppController) getApplication()).getMockup()){
                    if (params[1].equals("Gabriel Asensio Madrid")){
                        setRecibirAvisos(true);
                    }
                    else if(params[1].equals("Maria Esther Palacios Lorenzo")){
                        setRecibirAvisos(false);
                    }

                }
                /**************/
                else{
                    url = "http://192.168.1.107:1111/inicio/all" + params[1];
                            JsonObjectRequest jsonObjReq = new JsonObjectRequest(url, null,
                                    new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            Log.d(LOGTAG, "dentro de onResponse, url:" + url);
                                            try {
                                                data = response.getJSONArray("content");
                                                for (int i = 0; i < data.length(); i++) {
                                                    JSONObject obj = data.getJSONObject(i);
                                                    aviso = obj.getString("RecibirAviso");
                                                    if(aviso.equals("Sí"))
                                                        setRecibirAvisos(true);
                                                    else
                                                        setRecibirAvisos(false);
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e(LOGTAG, "dentro de onErrorResponse");
                                }
                            }) {
                                @Override
                                public String getBodyContentType() {
                                    return "application/json; charset=utf-8";
                                }
                            };
                            AppController.getInstance().addToRequestQueue(jsonObjReq);
                }
            }

            return null;
        }
    }
}
